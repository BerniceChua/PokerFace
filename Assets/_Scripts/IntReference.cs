﻿using System;

[Serializable]
public class IntReference
{
    public bool UseContant = true;
    public int ConstantValue;
    public IntVariable Variable;

    public int Value
    {
        get { return UseContant ? ConstantValue : Variable.Value; }
    }
}
