﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using NSubstitute;
using FluentAssertions;
using UnityEngine.TestTools;

public class GameManagerTests
{
    [Test]
    public void Test0_HowToCallScriptableObjectsInNotMonobehaviour()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        GameManagerScriptableObjectFullDeckOfCardsBuilder fullDeckOfCards = A.FullDeckOfCardsBuilder;
        #endregion

        #region ACT
        List<CardScriptableObject> fullDeck = fullDeckOfCards;
        int cardCount = fullDeck.Count;
        #endregion

        #region ASSERT
        //Assert.AreEqual(52, cardCount);
        cardCount.Should().Be(52);
        fullDeck[0].GetNumber.Should().Be(1);
        fullDeck[0].Suit.Should().Be(CardSuitsEnum.Clubs.ToString());
        fullDeck[12].GetNumber.Should().Be(13);
        fullDeck[12].Suit.Should().Be(CardSuitsEnum.Clubs.ToString());

        fullDeck[13].GetNumber.Should().Be(1);
        fullDeck[13].Suit.Should().Be(CardSuitsEnum.Diamonds.ToString());
        fullDeck[25].GetNumber.Should().Be(13);
        fullDeck[25].Suit.Should().Be(CardSuitsEnum.Diamonds.ToString());

        fullDeck[26].GetNumber.Should().Be(1);
        fullDeck[26].Suit.Should().Be(CardSuitsEnum.Hearts.ToString());
        fullDeck[38].GetNumber.Should().Be(13);
        fullDeck[38].Suit.Should().Be(CardSuitsEnum.Hearts.ToString());

        fullDeck[39].GetNumber.Should().Be(1);
        fullDeck[39].Suit.Should().Be(CardSuitsEnum.Spades.ToString());
        fullDeck[51].GetNumber.Should().Be(13);
        fullDeck[51].Suit.Should().Be(CardSuitsEnum.Spades.ToString());
        #endregion
    }

    [Test]
    public void Test1ThatEachPlayerGets5Cards()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager thisGameManager = new GameLoopManager();
        IGameLoopManagerBehaviour gameManagerBehaviour = Substitute.For<IGameLoopManagerBehaviour>();
        GameManagerScriptableObjectFullDeckOfCardsBuilder fullDeckOfCards = A.FullDeckOfCardsBuilder;

        Player thisPlayer = new Player();
        //PlayerScriptableObject playerSO = new PlayerScriptableObject();
        PlayerScriptableObject playerSO = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        //PlayerBehaviour playerBehaviour = new PlayerBehaviour();
        IPlayerBehaviour playerBehaviour = Substitute.For<IPlayerBehaviour>();

        //PlayerScriptableObjectOriginallyEmptyHandBuilder playerScriptableObj = A.OriginallyEmptyHand.SetPlayerName("Player 1");
        PlayerScriptableObject playerScriptableObj = A.OriginallyEmptyHandBuilder;
        #endregion

        #region ACT
        ////int cardCount = playerSO.m_PlayerHand.Count;
        ////gameManagerBehaviour.DistributeCards();
        //thisGameManager.DistributeCards(allCards, playerBehaviour, 0);
        //thisGameManager.DistributeCards(allCards, /*thisPlayer*/ playerBehaviour, 0);
        ////thisPlayer.AddCards(allCards, playerSO.m_PlayerHand);

        thisGameManager.Shuffle(fullDeckOfCards);

        playerScriptableObj.m_PlayerName = "Player 1";
        //thisGameManager.DistributeCards(fullDeckOfCards, playerBehaviour, 0);
        thisGameManager.DistributeCards(fullDeckOfCards, playerScriptableObj, 0);

        #endregion

        #region ASSERT
        //Assert.AreEqual(5, playerSO.m_PlayerHand.Count);
        //playerSO.m_PlayerHand.Count.Should().Be(5);
        playerScriptableObj.m_PlayerName.Should().Be("Player 1");
        //playerBehaviour.m_Player.m_PlayerScriptableObject.m_PlayerHand.Count.Should().Be(5);
        playerScriptableObj.m_PlayerHand.Count.Should().Be(5);
        #endregion
    }

    [Test]
    public void Test1ThatEachPlayerGetsDifferentCards()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();
        //gameManager.m_HandSize.ConstantValue = 5;

        GameManagerScriptableObjectFullDeckOfCardsBuilder fullDeckOfCards = A.FullDeckOfCardsBuilder;

        IGameLoopManagerBehaviour gameManagerBehaviour = Substitute.For<IGameLoopManagerBehaviour>();

        //int position = gameManager.m_HandSize.ConstantValue;
        PlayerScriptableObject player1 = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        PlayerScriptableObject player2 = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        #endregion

        #region ACT
        //for (int i = 0 + position; i < 5 + position; i++)
        //{
        //    Assert.AreNotEqual(, thisPlayer.);
        //}
        gameManager.DistributeCards(fullDeckOfCards, player1, 0);
        gameManager.DistributeCards(fullDeckOfCards, player2, 5);
        #endregion

        #region ASSERT
        player1.m_PlayerHand[0].Should().NotBeSameAs(player2.m_PlayerHand[0]);
        player1.m_PlayerHand[1].Should().NotBeSameAs(player2.m_PlayerHand[1]);
        player1.m_PlayerHand[2].Should().NotBe(player2.m_PlayerHand[2]);
        player1.m_PlayerHand[3].Should().NotBe(player2.m_PlayerHand[3]);
        player1.m_PlayerHand[4].Should().NotBe(player2.m_PlayerHand[4]);
        #endregion
    }

    [Test]
    public void Test2SortByAttackPower_WillSortByDescendingOrderWithAce()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject randomCardsWithAceHand = A.HighCardsWithAceHand2Builder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(randomCardsWithAceHand.m_PlayerHand);
        #endregion

        #region ASSERT
        #region old version
        //Assert.AreEqual(1, sortedHand[0].GetNumber);
        //Assert.AreEqual(12, sortedHand[1].GetNumber);
        //Assert.AreEqual(8, sortedHand[2].GetNumber);
        //Assert.AreEqual(5, sortedHand[3].GetNumber);
        //Assert.AreEqual(2, sortedHand[4].GetNumber);
        #endregion
        sortedHand[0].GetNumber.Should().Be(1);
        sortedHand[1].GetNumber.Should().Be(12);
        sortedHand[2].GetNumber.Should().Be(8);
        sortedHand[3].GetNumber.Should().Be(5);
        sortedHand[4].GetNumber.Should().Be(2);
        #endregion
    }

    [Test]
    public void Test2SortByAttackPower_WillSortByDescendingOrderNoAce()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject highCardNoAceHand = A.HighCardNoAceHand1Builder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(highCardNoAceHand.m_PlayerHand);
        #endregion

        #region ASSERT
        #region old version
        //Assert.AreEqual(12, sortedHand[0].GetNumber);
        //Assert.AreEqual(8, sortedHand[1].GetNumber);
        //Assert.AreEqual(7, sortedHand[2].GetNumber);
        //Assert.AreEqual(5, sortedHand[3].GetNumber);
        //Assert.AreEqual(2, sortedHand[4].GetNumber);
        #endregion
        sortedHand[0].GetNumber.Should().Be(12);
        sortedHand[1].GetNumber.Should().Be(8);
        sortedHand[2].GetNumber.Should().Be(7);
        sortedHand[3].GetNumber.Should().Be(5);
        sortedHand[4].GetNumber.Should().Be(2);
        #endregion
    }

    [Test]
    public void Test2SortByCardNumber_WillSortByDescendingOrderWithAce()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject randomCardsWithAceHand = A.HighCardsWithAceHand2Builder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByCardNumber(randomCardsWithAceHand.m_PlayerHand);
        #endregion

        #region ASSERT
        #region old version
        //Assert.AreEqual(12, sortedHand[0].GetNumber);
        //Assert.AreEqual(8, sortedHand[1].GetNumber);
        //Assert.AreEqual(5, sortedHand[2].GetNumber);
        //Assert.AreEqual(2, sortedHand[3].GetNumber);
        //Assert.AreEqual(1, sortedHand[4].GetNumber);
        #endregion
        sortedHand[0].GetNumber.Should().Be(12);
        sortedHand[1].GetNumber.Should().Be(8);
        sortedHand[2].GetNumber.Should().Be(5);
        sortedHand[3].GetNumber.Should().Be(2);
        sortedHand[4].GetNumber.Should().Be(1);
        #endregion
    }

    [Test]
    public void Test2SortByCardNumber_WillSortByDescendingOrderNoAce()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObjectHighCardNoAceHand1Builder highCardNoAceHand = A.HighCardNoAceHand1Builder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByCardNumber(highCardNoAceHand.HighCardNoAceHand1());
        #endregion

        #region ASSERT
        #region old version
        //Assert.AreEqual(12, sortedHand[0].GetNumber);
        //Assert.AreEqual(8, sortedHand[1].GetNumber);
        //Assert.AreEqual(7, sortedHand[2].GetNumber);
        //Assert.AreEqual(5, sortedHand[3].GetNumber);
        //Assert.AreEqual(2, sortedHand[4].GetNumber);
        #endregion
        sortedHand[0].GetNumber.Should().Be(12);
        sortedHand[1].GetNumber.Should().Be(8);
        sortedHand[2].GetNumber.Should().Be(7);
        sortedHand[3].GetNumber.Should().Be(5);
        sortedHand[4].GetNumber.Should().Be(2);
        #endregion
    }

    [Test]
    public void TestCheckIfFlush_CardsHaveSameSuits_ReturnTrue()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject allSameSuitsHand = A.AllSameSuitsHand;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByCardNumber(allSameSuitsHand.m_PlayerHand);
        allSameSuitsHand.m_PlayerHand = sortedHand;
        bool bIsFlushHand = gameManager.CheckIfFlush(allSameSuitsHand);
        #endregion

        #region ASSERT
        //Assert.AreEqual(true, bIsFlushHand);
        bIsFlushHand.Should().Be(true);
        #endregion
    }

    [Test]
    public void TestCheckIfFlush_CardsHaveDifferentSuits_ReturnFalse()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject highCardNoAceHand = A.HighCardNoAceHand1Builder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByCardNumber(highCardNoAceHand.m_PlayerHand);
        highCardNoAceHand.m_PlayerHand = sortedHand;
        bool bIsFlushHand = gameManager.CheckIfFlush(highCardNoAceHand);
        #endregion

        #region ASSERT
        //Assert.AreEqual(false, bIsFlushHand);
        bIsFlushHand.Should().Be(false);
        #endregion
    }

    [Test]
    public void TestCheckIfStraight_BroadwayStraight_ReturnTrue()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject broadwayStraightHand = A.BroadwayStraightHand;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(broadwayStraightHand.m_PlayerHand);
        broadwayStraightHand.m_PlayerHand = sortedHand;
        bool bIsBroadwayStraight = gameManager.CheckIfStraight(broadwayStraightHand);
        #endregion

        #region ASSERT
        //Assert.AreEqual(true, bIsBroadwayStraight);
        bIsBroadwayStraight.Should().Be(true);
        #endregion
    }

    [Test]
    public void TestCheckIfStraight_BabyStraight_ReturnTrue()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject babyStraightHand = A.BabyStraightHand;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(babyStraightHand.m_PlayerHand);
        babyStraightHand.m_PlayerHand = sortedHand;
        bool bIsBabyStraight = gameManager.CheckIfStraight(babyStraightHand);
        #endregion

        #region ASSERT
        //Assert.AreEqual(true, bIsBabyStraight);
        bIsBabyStraight.Should().Be(true);
        #endregion
    }

    [Test]
    public void TestCheckIfStraight_NonSpecialStraight1_ReturnTrue()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject nonSpecialStraight1Hand = A.NonSpecialStraight1Hand;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(nonSpecialStraight1Hand.m_PlayerHand);
        nonSpecialStraight1Hand.m_PlayerHand = sortedHand;
        bool bNonSpecialStraight1 = gameManager.CheckIfStraight(nonSpecialStraight1Hand);
        #endregion

        #region ASSERT
        //Assert.AreEqual(true, bNonSpecialStraight1);
        bNonSpecialStraight1.Should().Be(true);
        #endregion
    }

    [Test]
    public void TestCheckIfStraight_NonSpecialStraight2_ReturnTrue()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject nonSpecialStraight2Hand = A.NonSpecialStraight2Hand;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(nonSpecialStraight2Hand.m_PlayerHand);
        nonSpecialStraight2Hand.m_PlayerHand = sortedHand;
        bool bNonSpecialStraight2 = gameManager.CheckIfStraight(nonSpecialStraight2Hand);
        #endregion

        #region ASSERT
        //Assert.AreEqual(true, bNonSpecialStraight2);
        bNonSpecialStraight2.Should().Be(true);
        #endregion
    }

    [Test]
    public void TestCheckIfStraight_HighCard_ReturnFalse()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject randomCardsHand = A.HighCardsWithAceHand2Builder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(randomCardsHand.m_PlayerHand);
        randomCardsHand.m_PlayerHand = sortedHand;
        bool bIsStraightHand = gameManager.CheckIfStraight(randomCardsHand);
        #endregion

        #region ASSERT
        //Assert.AreEqual(false, bIsStraightHand);
        bIsStraightHand.Should().Be(false);
        #endregion
    }

    [Test]
    public void TestCheckIfStraight_SequencialExceptLastCard_ReturnFalse()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject sequencialExceptLastCardHand = A.SequencialExceptLastCardHand;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(sequencialExceptLastCardHand.m_PlayerHand);
        sequencialExceptLastCardHand.m_PlayerHand = sortedHand;
        bool bIsStraightHand = gameManager.CheckIfStraight(sequencialExceptLastCardHand);
        #endregion

        #region ASSERT
        //Assert.AreEqual(false, bIsStraightHand);
        bIsStraightHand.Should().Be(false);
        #endregion
    }

    [Test]
    public void TestCheckIfStraightFlush_StraightFlushHand_ReturnTrue()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject nonSpecialStraightFlushHand = A.NonSpecialStraightFlushHand;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(nonSpecialStraightFlushHand.m_PlayerHand);
        nonSpecialStraightFlushHand.m_PlayerHand = sortedHand;
        bool bIsStraightFlushHand = gameManager.CheckIfStraightFlush(nonSpecialStraightFlushHand);
        #endregion

        #region ASSERT
        //Assert.AreEqual(true, bIsStraightFlushHand);
        bIsStraightFlushHand.Should().Be(true);
        #endregion
    }

    [Test]
    public void TestCheckIfStraightFlush_BroadwayStraightFlushHand_ReturnTrue()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject broadwayStraightFlushHand = A.BroadwayStraightFlushHand;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(broadwayStraightFlushHand.m_PlayerHand);
        broadwayStraightFlushHand.m_PlayerHand = sortedHand;
        bool bIsStraightFlushHand = gameManager.CheckIfStraightFlush(broadwayStraightFlushHand);
        #endregion

        #region ASSERT
        //Assert.AreEqual(true, bIsStraightFlushHand);
        bIsStraightFlushHand.Should().Be(true);
        #endregion
    }

    [Test]
    public void TestCheckIfStraightFlush_BabyStraightFlushHand_ReturnTrue()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject babyStraightFlushHand = A.BabyStraightFlushHand;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(babyStraightFlushHand.m_PlayerHand);
        babyStraightFlushHand.m_PlayerHand = sortedHand;
        bool bIsStraightFlushHand = gameManager.CheckIfStraightFlush(babyStraightFlushHand);
        #endregion

        #region ASSERT
        //Assert.AreEqual(true, bIsStraightFlushHand);
        bIsStraightFlushHand.Should().Be(true);
        #endregion
    }

    [Test]
    public void TestCheckIfStraightFlush_HighCard_ReturnFalse()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject randomCardsHand = A.HighCardsWithAceHand2Builder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(randomCardsHand.m_PlayerHand);
        randomCardsHand.m_PlayerHand = sortedHand;
        bool bIsStraightFlushHand = gameManager.CheckIfStraightFlush(randomCardsHand);
        #endregion

        #region ASSERT
        //Assert.AreEqual(false, bIsStraightFlushHand);
        bIsStraightFlushHand.Should().Be(false);
        #endregion
    }

    [Test]
    public void TestCheckIfFourOfAKind_FourSameFront_ReturnTrue()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject fourSameCardsHand = A.FourSameCardsHand;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(fourSameCardsHand.m_PlayerHand);
        fourSameCardsHand.m_PlayerHand = sortedHand;
        bool bIsFourOfAKind = gameManager.CheckIfFourOfAKind(fourSameCardsHand);
        #endregion

        #region ASSERT
        //Assert.AreEqual(true, bIsFourOfAKind);
        bIsFourOfAKind.Should().Be(true);
        #endregion
    }

    [Test]
    public void TestCheckIfFourOfAKind_FourSameBack_ReturnTrue()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject fourSameCardsHand = A.FourLastSameCardsHandBuilder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(fourSameCardsHand.m_PlayerHand);
        fourSameCardsHand.m_PlayerHand = sortedHand;
        bool bIsFourOfAKind = gameManager.CheckIfFourOfAKind(fourSameCardsHand);
        #endregion

        #region ASSERT
        bIsFourOfAKind.Should().Be(true);
        #endregion
    }

    [Test]
    public void TestCheckIfFourOfAKind_HighCard_ReturnFalse()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject highCardNoAceHand = A.HighCardNoAceHand1Builder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(highCardNoAceHand.m_PlayerHand);
        highCardNoAceHand.m_PlayerHand = sortedHand;
        bool bIsFourOfAKind = gameManager.CheckIfFourOfAKind(highCardNoAceHand);
        #endregion

        #region ASSERT
        //Assert.AreEqual(false, bIsFourOfAKind);
        bIsFourOfAKind.Should().Be(false);
        #endregion
    }

    [Test]
    public void TestCheckIfFourOfAKind_ThreeSame_ReturnFalse()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject threeSameCardsHand = A.ThreeSameFrontCardsHandBuilder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(threeSameCardsHand.m_PlayerHand);
        threeSameCardsHand.m_PlayerHand = sortedHand;
        bool bIsThreeOfAKind = gameManager.CheckIfFourOfAKind(threeSameCardsHand);
        #endregion

        #region ASSERT
        bIsThreeOfAKind.Should().Be(false);
        #endregion
    }

    [Test]
    public void TestCheckIfFullHouse_FullHouse3InFront_ReturnTrue()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject fullHouseHand = A.FullHouseHand;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(fullHouseHand.m_PlayerHand);
        fullHouseHand.m_PlayerHand = sortedHand;
        bool bIsFullHouse = gameManager.CheckIfThreeOfAKind(fullHouseHand);
        #endregion

        #region ASSERT
        bIsFullHouse.Should().Be(true);
        #endregion
    }

    [Test]
    public void TestCheckIfFullHouse_FullHouse3InBack_ReturnTrue()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject fullHouseHand = A.FullHouseLast3HandBuilder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(fullHouseHand.m_PlayerHand);
        fullHouseHand.m_PlayerHand = sortedHand;
        bool bIsFullHouse = gameManager.CheckIfThreeOfAKind(fullHouseHand);
        #endregion

        #region ASSERT
        bIsFullHouse.Should().Be(true);
        #endregion
    }

    [Test]
    public void TestCheckIfFullHouse_ThreeSame_ReturnFalse()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject threeSameCardsHand = A.ThreeSameFrontCardsHandBuilder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(threeSameCardsHand.m_PlayerHand);
        threeSameCardsHand.m_PlayerHand = sortedHand;
        bool bIsFullHouse = gameManager.CheckIfFullHouse(threeSameCardsHand);
        #endregion

        #region ASSERT
        bIsFullHouse.Should().Be(false);
        #endregion
    }

    [Test]
    public void TestCheckIfFullHouse_HighCard_ReturnFalse()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject highCardNoAceHand = A.HighCardNoAceHand1Builder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(highCardNoAceHand.m_PlayerHand);
        highCardNoAceHand.m_PlayerHand = sortedHand;
        bool bIsFullHouse = gameManager.CheckIfFullHouse(highCardNoAceHand);
        #endregion

        #region ASSERT
        bIsFullHouse.Should().Be(false);
        #endregion
    }

    [Test]
    public void TestCheckIfThreeOfAKind_ThreeSameFront_ReturnTrue()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject threeSameCardsHand = A.ThreeSameFrontCardsHandBuilder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(threeSameCardsHand.m_PlayerHand);
        threeSameCardsHand.m_PlayerHand = sortedHand;
        bool bIsThreeOfAKind = gameManager.CheckIfThreeOfAKind(threeSameCardsHand);
        #endregion

        #region ASSERT
        bIsThreeOfAKind.Should().Be(true);
        #endregion
    }

    [Test]
    public void TestCheckIfThreeOfAKind_ThreeSameMiddle_ReturnTrue()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject threeSameCardsHand = A.ThreeSameMiddleCardsHandBuilder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(threeSameCardsHand.m_PlayerHand);
        threeSameCardsHand.m_PlayerHand = sortedHand;
        bool bIsThreeOfAKind = gameManager.CheckIfThreeOfAKind(threeSameCardsHand);
        #endregion

        #region ASSERT
        bIsThreeOfAKind.Should().Be(true);
        #endregion
    }

    [Test]
    public void TestCheckIfThreeOfAKind_ThreeSameLast_ReturnTrue()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject threeSameCardsHand = A.ThreeSameMiddleCardsHandBuilder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(threeSameCardsHand.m_PlayerHand);
        threeSameCardsHand.m_PlayerHand = sortedHand;
        bool bIsThreeOfAKind = gameManager.CheckIfThreeOfAKind(threeSameCardsHand);
        #endregion

        #region ASSERT
        bIsThreeOfAKind.Should().Be(true);
        #endregion
    }

    [Test]
    public void TestCheckIfThreeOfAKind_RandomCards_ReturnFalse()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject highCardNoAceHand = A.HighCardNoAceHand1Builder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(highCardNoAceHand.m_PlayerHand);
        highCardNoAceHand.m_PlayerHand = sortedHand;
        bool bIsThreeOfAKind = gameManager.CheckIfThreeOfAKind(highCardNoAceHand);
        #endregion

        #region ASSERT
        //Assert.AreEqual(true, bIsThreeOfAKind);
        bIsThreeOfAKind.Should().Be(false);
        #endregion
    }

    [Test]
    public void TestCheckIfTwoPairs_TwoFrontPairs_ReturnTrue()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject twoPairsHand = A.TwoPairFrontHand;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(twoPairsHand.m_PlayerHand);

        bool bIsTwoPairsHand = gameManager.CheckIf2Pairs(twoPairsHand);
        #endregion

        #region ASSERT
        bIsTwoPairsHand.Should().Be(true);
        #endregion
    }

    [Test]
    public void TestCheckIfTwoPairs_TwoLastPairs_ReturnTrue()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject twoPairsHand = A.TwoPairLastHand;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(twoPairsHand.m_PlayerHand);
        twoPairsHand.m_PlayerHand = sortedHand;
        bool bIsTwoPairsHand = gameManager.CheckIf2Pairs(twoPairsHand);
        #endregion

        #region ASSERT
        bIsTwoPairsHand.Should().Be(true);
        #endregion
    }

    [Test]
    public void TestCheckIfTwoPairs_TwoSplitPairs_ReturnTrue()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject twoPairsHand = A.TwoPairSplitHand;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(twoPairsHand.m_PlayerHand);
        twoPairsHand.m_PlayerHand = sortedHand;
        bool bIsTwoPairsHand = gameManager.CheckIf2Pairs(twoPairsHand);
        #endregion

        #region ASSERT
        bIsTwoPairsHand.Should().Be(true);
        #endregion
    }

    [Test]
    public void TestCheckIfTwoPairs_HighCard_ReturnFalse()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject highCardNoAceHand = A.HighCardNoAceHand1Builder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(highCardNoAceHand.m_PlayerHand);
        bool bIsTwoPairsHand = gameManager.CheckIf2Pairs(highCardNoAceHand);
        #endregion

        #region ASSERT
        //Assert.AreEqual(true, bIsTwoPairsHand);
        bIsTwoPairsHand.Should().Be(false);
        #endregion
    }

    [Test]
    public void TestCheckIfTwoPairs_OnePair_ReturnFalse()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject onePairHand = A.OnePairHandBuilder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(onePairHand.m_PlayerHand);
        bool bIsTwoPairsHand = gameManager.CheckIf2Pairs(onePairHand);
        #endregion

        #region ASSERT
        bIsTwoPairsHand.Should().Be(false);
        #endregion
    }

    [Test]
    public void TestCheckIfOnePair_OnePair1st_ReturnTrue()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject onePair1st = A.OnePairOf10HandBuilder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(onePair1st.m_PlayerHand);
        //bool bIsOnePairHand = gameManager.CheckIfOnePair(onePairHand, sortedHand);
        onePair1st.m_PlayerHand = sortedHand;
        bool bIsOnePairHand = gameManager.CheckIfOnePair(onePair1st/*, sortedHand*/);
        #endregion

        #region ASSERT
        onePair1st.GetHandTypePowerLevel.Should().Be(10);

        bIsOnePairHand.Should().Be(true);
        #endregion
    }

    [Test]
    public void TestCheckIfOnePair_OnePairMiddle1_ReturnTrue()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject onePairHand = A.OnePairHandBuilder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(onePairHand.m_PlayerHand);
        //bool bIsOnePairHand = gameManager.CheckIfOnePair(onePairHand, sortedHand);
        onePairHand.m_PlayerHand = sortedHand;
        bool bIsOnePairHand = gameManager.CheckIfOnePair(onePairHand/*, sortedHand*/);
        #endregion

        #region ASSERT
        onePairHand.GetHandTypePowerLevel.Should().Be(8);

        bIsOnePairHand.Should().Be(true);
        #endregion
    }

    [Test]
    public void TestCheckIfOnePair_OnePairMiddle2_ReturnTrue()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject onePairMiddle2 = A.OnePairOf5HandBuilder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(onePairMiddle2.m_PlayerHand);
        //bool bIsOnePairHand = gameManager.CheckIfOnePair(onePairHand, sortedHand);
        onePairMiddle2.m_PlayerHand = sortedHand;
        bool bIsOnePairHand = gameManager.CheckIfOnePair(onePairMiddle2/*, sortedHand*/);
        #endregion

        #region ASSERT
        onePairMiddle2.GetHandTypePowerLevel.Should().Be(5);

        bIsOnePairHand.Should().Be(true);
        #endregion
    }

    [Test]
    public void TestCheckIfOnePair_OnePairLast_ReturnTrue()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject onePairLast = A.OnePairOf2Hand1stBuilder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(onePairLast.m_PlayerHand);
        //bool bIsOnePairHand = gameManager.CheckIfOnePair(onePairHand, sortedHand);
        onePairLast.m_PlayerHand = sortedHand;
        bool bIsOnePairHand = gameManager.CheckIfOnePair(onePairLast/*, sortedHand*/);
        #endregion

        #region ASSERT
        onePairLast.GetHandTypePowerLevel.Should().Be(2);

        bIsOnePairHand.Should().Be(true);
        #endregion
    }

    [Test]
    public void TestCheckIfOnePair_HighCard_ReturnFalse()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject highCardNoAceHand = A.HighCardNoAceHand1Builder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHand = gameManager.SortByAttackPower(highCardNoAceHand.m_PlayerHand);
        highCardNoAceHand.m_PlayerHand = sortedHand;
        bool bIsOnePairHand = gameManager.CheckIfOnePair(highCardNoAceHand/*, sortedHand*/);
        #endregion

        #region ASSERT
        //Assert.AreEqual(true, bIsTwoPairsHand);
        bIsOnePairHand.Should().Be(false);
        #endregion
    }

    [Test]
    public void TestCheckIfOnePair_TieBreaker_WithWinner()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject player1OnePairOf8Hand = A.OnePairOf8HandBuilder;
        PlayerScriptableObject player2OnePairOf10Hand = A.OnePairOf10HandBuilder;
        player1OnePairOf8Hand.m_PlayerName = "Player 1, Pair Of 8";
        player2OnePairOf10Hand.m_PlayerName = "Player 2, Pair Of 10";

        //PlayerScriptableObject player1 = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        //player1.m_PlayerHand = player1OnePairOf8Hand.m_PlayerHand;
        //UnityEngine.Debug.Log("player1.m_PlayerHand.Count = " + player1.m_PlayerHand.Count);

        //PlayerScriptableObject player2 = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        //player2.m_PlayerHand = player2OnePairOf10Hand.m_PlayerHand;
        #endregion

        #region ACT
        List<CardScriptableObject> sorted1PairOf8Hand = gameManager.SortByAttackPower(player1OnePairOf8Hand.m_PlayerHand);
        player1OnePairOf8Hand.m_PlayerHand = sorted1PairOf8Hand;
        bool bIsPlayer1OnePair = gameManager.CheckIfOnePair(player1OnePairOf8Hand/*, sorted1PairOf8Hand*/);

        List<CardScriptableObject> sorted1PairOf10Hand = gameManager.SortByAttackPower(player2OnePairOf10Hand.m_PlayerHand);
        player2OnePairOf10Hand.m_PlayerHand = sorted1PairOf10Hand;
        bool bIsPlayer2OnePair = gameManager.CheckIfOnePair(player2OnePairOf10Hand/*, sorted1PairOf8Hand*/);
        #endregion

        #region ASSERT
        bIsPlayer1OnePair.Should().Be(true);
        bIsPlayer2OnePair.Should().Be(true);

        player1OnePairOf8Hand.GetHandTypePowerLevel.Should().Be(8);
        player2OnePairOf10Hand.GetHandTypePowerLevel.Should().Be(10);

        gameManager.TieBreaker(player1OnePairOf8Hand, player2OnePairOf10Hand).Should().Be(player2OnePairOf10Hand);

        gameManager.Compare2Hands(player1OnePairOf8Hand, player2OnePairOf10Hand).Should().Be(player2OnePairOf10Hand);
        #endregion
    }

    [Test]
    public void TestCheckIfOnePair_TieBreaker_TiedWithoutWinner()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject player1OnePairOf2Hand = A.OnePairOf2Hand1stBuilder;
        PlayerScriptableObject player2OnePairOf2Hand = A.OnePairOf2Hand2ndBuilder;
        player1OnePairOf2Hand.m_PlayerName = "Player 1, Pair Of 2";
        player2OnePairOf2Hand.m_PlayerName = "Player 2, Pair Of 2";
        #endregion

        #region ACT
        List<CardScriptableObject> sorted1PairOf8Hand = gameManager.SortByAttackPower(player1OnePairOf2Hand.m_PlayerHand);
        player1OnePairOf2Hand.m_PlayerHand = sorted1PairOf8Hand;
        bool bIsPlayer1OnePair = gameManager.CheckIfOnePair(player1OnePairOf2Hand/*, sorted1PairOf8Hand*/);

        List<CardScriptableObject> sorted1PairOf10Hand = gameManager.SortByAttackPower(player2OnePairOf2Hand.m_PlayerHand);
        player2OnePairOf2Hand.m_PlayerHand = sorted1PairOf10Hand;
        bool bIsPlayer2OnePair = gameManager.CheckIfOnePair(player2OnePairOf2Hand/*, sorted1PairOf8Hand*/);
        #endregion

        #region ASSERT
        bIsPlayer1OnePair.Should().Be(true);
        bIsPlayer2OnePair.Should().Be(true);

        player1OnePairOf2Hand.GetHandTypePowerLevel.Should().Be(2);
        player2OnePairOf2Hand.GetHandTypePowerLevel.Should().Be(2);

        gameManager.TieBreaker(player1OnePairOf2Hand, player2OnePairOf2Hand).Should().Be(null);

        gameManager.Compare2Hands(player1OnePairOf2Hand, player2OnePairOf2Hand).Should().Be(null);
        #endregion
    }

    [Test]
    public void TestCompare2HighCards_LastNumberDifference()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject player1 = A.HighCardNoAceHand1Builder;
        PlayerScriptableObject player2 = A.HighCardNoAceHand2Builder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHandPlayer1 = gameManager.SortByAttackPower(player1.m_PlayerHand);
        player1.m_PlayerHand = sortedHandPlayer1;
        //gameManager.DistributeCards(sortedHandPlayer1, player1, 0);

        List<CardScriptableObject> sortedHandPlayer2 = gameManager.SortByAttackPower(player2.m_PlayerHand);
        player2.m_PlayerHand = sortedHandPlayer2;
        //gameManager.DistributeCards(sortedHandPlayer2, player2, 0);
        #endregion

        #region ASSERT
        gameManager.CompareHighCards(player1, player2).Should().Be(player2);

        gameManager.Compare2Hands(player1, player2).Should().Be(player2);
        #endregion
    }

    [Test]
    public void TestCompare2HighCards_1stCardOnlySame()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject player1 = A.HighCardsWithAceHand1Builder;
        PlayerScriptableObject player2 = A.HighCardsWithAceHand2Builder;
        #endregion

        #region ACT
        List<CardScriptableObject> sortedHandPlayer1 = gameManager.SortByAttackPower(player1.m_PlayerHand);
        player1.m_PlayerHand = sortedHandPlayer1;
        //gameManager.DistributeCards(sortedHandPlayer1, player1, 0);

        List<CardScriptableObject> sortedHandPlayer2 = gameManager.SortByAttackPower(player2.m_PlayerHand);
        player2.m_PlayerHand = sortedHandPlayer2;
        //gameManager.DistributeCards(sortedHandPlayer2, player2, 0);
        #endregion

        #region ASSERT
        gameManager.CompareHighCards(player1, player2).Should().Be(player2);

        gameManager.Compare2Hands(player1, player2).Should().Be(player2);
        #endregion
    }

    [Test]
    public void TestTieBreaker_2HandsOfOnePair_WithWinner()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager gameManager = new GameLoopManager();

        PlayerScriptableObject player1OnePairOf10Hand = A.OnePairOf10HandBuilder;
        player1OnePairOf10Hand.m_PlayerName = "Player 1, Pair Of 10";

        PlayerScriptableObject player2OnePairOf2Hand = A.OnePairOf2Hand1stBuilder;
        player2OnePairOf2Hand.m_PlayerName = "Player 2, Pair Of 2";
        #endregion

        #region ACT
        List<CardScriptableObject> sorted1PairOf10Hand = gameManager.SortByAttackPower(player1OnePairOf10Hand.m_PlayerHand);
        player1OnePairOf10Hand.m_PlayerHand = sorted1PairOf10Hand;
        bool bIsPlayer2OnePair = gameManager.CheckIfOnePair(player1OnePairOf10Hand/*, sorted1PairOf8Hand*/);

        List<CardScriptableObject> sorted1PairOf2Hand = gameManager.SortByAttackPower(player2OnePairOf2Hand.m_PlayerHand);
        player2OnePairOf2Hand.m_PlayerHand = sorted1PairOf2Hand;
        bool bIsPlayer1OnePair = gameManager.CheckIfOnePair(player2OnePairOf2Hand/*, sorted1PairOf8Hand*/);
        #endregion

        #region ASSERT
        bIsPlayer1OnePair.Should().Be(true);
        bIsPlayer2OnePair.Should().Be(true);

        gameManager.TieBreaker(player2OnePairOf2Hand, player1OnePairOf10Hand).Should().Be(player1OnePairOf10Hand);

        gameManager.Compare2Hands(player2OnePairOf2Hand, player1OnePairOf10Hand).Should().Be(player1OnePairOf10Hand);

        player1OnePairOf10Hand.GetHandTypePowerLevel.Should().Be(10);
        player1OnePairOf10Hand.m_HandType.Should().Be("One Pair");
        player1OnePairOf10Hand.PowerTier.Should().Be(2);

        player2OnePairOf2Hand.GetHandTypePowerLevel.Should().Be(2);
        player2OnePairOf2Hand.m_HandType.Should().Be("One Pair");
        player2OnePairOf2Hand.PowerTier.Should().Be(2);
        #endregion
    }

}
