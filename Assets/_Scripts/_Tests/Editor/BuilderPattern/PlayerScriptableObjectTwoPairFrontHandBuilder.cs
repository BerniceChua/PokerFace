﻿using System.Collections.Generic;

public class PlayerScriptableObjectTwoPairFrontHandBuilder
{
    public List<CardScriptableObject> TwoFrontSameHand()
    {
        CardScriptableObject eightOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        eightOfHearts.NumberSetterForTests(8);
        eightOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject eightOfSpades = CardScriptableObject.CreateInstance<CardScriptableObject>();
        eightOfSpades.NumberSetterForTests(8);
        eightOfSpades.CardSuitSetterForTests(CardSuitsEnum.Spades);

        CardScriptableObject fiveOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        fiveOfDiamonds.NumberSetterForTests(5);
        fiveOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject twoOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfDiamonds.NumberSetterForTests(2);
        twoOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject fiveOfClubs = CardScriptableObject.CreateInstance<CardScriptableObject>();
        fiveOfClubs.NumberSetterForTests(5);
        fiveOfClubs.CardSuitSetterForTests(CardSuitsEnum.Clubs);

        return new List<CardScriptableObject>()
        {
            twoOfDiamonds, fiveOfDiamonds, fiveOfClubs, eightOfSpades,  eightOfHearts
        };
    }

    public PlayerScriptableObject Build()
    {
        PlayerScriptableObject properties = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        properties.m_PlayerHand = TwoFrontSameHand();

        return properties;
    }

    public static implicit operator PlayerScriptableObject(PlayerScriptableObjectTwoPairFrontHandBuilder builder)
    {
        return builder.Build();
    }
}
