﻿using System.Collections.Generic;

public class PlayerScriptableObjectOriginallyEmptyHandBuilder
{
    private string m_playerName;
    private List<CardScriptableObject> m_playerHand;

    public PlayerScriptableObjectOriginallyEmptyHandBuilder SetPlayerName(string playerName)
    {
        m_playerName = playerName;
        return this;
    }

    public void AddCard(CardScriptableObject card, List<CardScriptableObject> cardsInHand)
    {
        cardsInHand.Add(card);
    }

    public PlayerScriptableObjectOriginallyEmptyHandBuilder SetPlayerHand(List<CardScriptableObject> cards)
    {
        m_playerHand = new List<CardScriptableObject>(cards);
        return this;
    }

    //public List<CardScriptableObject> OriginallyEmptyHand()
    //{
    //    CardScriptableObject sevenOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
    //    sevenOfHearts.NumberSetterForTests(7);
    //    sevenOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

    //    CardScriptableObject queenOfSpades = CardScriptableObject.CreateInstance<CardScriptableObject>();
    //    queenOfSpades.NumberSetterForTests(12);
    //    queenOfSpades.CardSuitSetterForTests(CardSuitsEnum.Spades);

    //    CardScriptableObject eightOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
    //    eightOfDiamonds.NumberSetterForTests(8);
    //    eightOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

    //    CardScriptableObject twoOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
    //    twoOfDiamonds.NumberSetterForTests(2);
    //    twoOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

    //    CardScriptableObject fiveOfClubs = CardScriptableObject.CreateInstance("CardScriptableObject") as CardScriptableObject;
    //    fiveOfClubs.NumberSetterForTests(5);
    //    fiveOfClubs.CardSuitSetterForTests(CardSuitsEnum.Clubs);

    //    return new List<CardScriptableObject>()
    //    {
    //        twoOfDiamonds, eightOfDiamonds, fiveOfClubs, queenOfSpades,  sevenOfHearts
    //    };
    //}

    public PlayerScriptableObject Build()
    {
        var thisPlayerScriptableObject = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        //m_playerHand = OriginallyEmptyHand();
        //thisPlayerScriptableObject.m_PlayerName = m_playerName;
        thisPlayerScriptableObject.SetPrivate(pso => pso.m_PlayerName, m_playerName);
        //thisPlayerScriptableObject.m_PlayerHand = m_playerHand;
        thisPlayerScriptableObject.SetPrivate(pso => pso.m_PlayerHand, m_playerHand);

        return thisPlayerScriptableObject;
    }

    public static implicit operator PlayerScriptableObject(PlayerScriptableObjectOriginallyEmptyHandBuilder builder)
    {
        return builder.Build();
    }
}
