﻿using System.Collections.Generic;

public class PlayerScriptableObjectBroadwayStraightFlushHandBuilder
{
    public List<CardScriptableObject> BroadwayStraightFlushHand()
    {
        CardScriptableObject aceOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        aceOfHearts.NumberSetterForTests(1);
        aceOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject queenOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        queenOfHearts.NumberSetterForTests(12);
        queenOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject jackOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        jackOfHearts.NumberSetterForTests(11);
        jackOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject tenOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        tenOfHearts.NumberSetterForTests(10);
        tenOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject kingOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        kingOfHearts.NumberSetterForTests(13);
        kingOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        return new List<CardScriptableObject>()
        {
            tenOfHearts, jackOfHearts, kingOfHearts, queenOfHearts,  aceOfHearts
        };
    }

    public PlayerScriptableObject Build()
    {
        PlayerScriptableObject properties = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        properties.m_PlayerHand = BroadwayStraightFlushHand();

        return properties;
    }

    public static implicit operator PlayerScriptableObject(PlayerScriptableObjectBroadwayStraightFlushHandBuilder builder)
    {
        return builder.Build();
    }
}
