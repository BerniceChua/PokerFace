﻿using System.Collections.Generic;

public class PlayerScriptableObjectThreeSameMiddleCardsHandBuilder
{
    public List<CardScriptableObject> ThreeSameMiddleHand()
    {
        CardScriptableObject eightOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        eightOfHearts.NumberSetterForTests(8);
        eightOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject fiveOfSpades = CardScriptableObject.CreateInstance<CardScriptableObject>();
        fiveOfSpades.NumberSetterForTests(5);
        fiveOfSpades.CardSuitSetterForTests(CardSuitsEnum.Spades);

        CardScriptableObject fiveOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        fiveOfDiamonds.NumberSetterForTests(5);
        fiveOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject twoOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfDiamonds.NumberSetterForTests(2);
        twoOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject fiveOfClubs = CardScriptableObject.CreateInstance<CardScriptableObject>();
        fiveOfClubs.NumberSetterForTests(5);
        fiveOfClubs.CardSuitSetterForTests(CardSuitsEnum.Clubs);

        return new List<CardScriptableObject>()
        {
            twoOfDiamonds, fiveOfDiamonds, fiveOfClubs, fiveOfSpades,  eightOfHearts
        };
    }

    public PlayerScriptableObject Build()
    {
        PlayerScriptableObject properties = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        properties.m_PlayerHand = ThreeSameMiddleHand();

        return properties;
    }

    public static implicit operator PlayerScriptableObject(PlayerScriptableObjectThreeSameMiddleCardsHandBuilder builder)
    {
        return builder.Build();
    }
}
