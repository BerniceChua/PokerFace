﻿using System.Collections.Generic;

public class PlayerScriptableObjectNonSpecialStraight1HandBuilder
{
    public List<CardScriptableObject> NonSpecialStraight1()
    {
        CardScriptableObject eightOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        eightOfHearts.NumberSetterForTests(8);
        eightOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject nineOfSpades = CardScriptableObject.CreateInstance<CardScriptableObject>();
        nineOfSpades.NumberSetterForTests(9);
        nineOfSpades.CardSuitSetterForTests(CardSuitsEnum.Spades);

        CardScriptableObject sevenOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        sevenOfDiamonds.NumberSetterForTests(7);
        sevenOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject tenOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        tenOfDiamonds.NumberSetterForTests(10);
        tenOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject sixOfClubs = CardScriptableObject.CreateInstance<CardScriptableObject>();
        sixOfClubs.NumberSetterForTests(6);
        sixOfClubs.CardSuitSetterForTests(CardSuitsEnum.Clubs);

        return new List<CardScriptableObject>()
        {
            tenOfDiamonds, sevenOfDiamonds, sixOfClubs, nineOfSpades,  eightOfHearts
        };
    }

    public PlayerScriptableObject Build()
    {
        PlayerScriptableObject properties = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        properties.m_PlayerHand = NonSpecialStraight1();

        return properties;
    }

    public static implicit operator PlayerScriptableObject(PlayerScriptableObjectNonSpecialStraight1HandBuilder builder)
    {
        return builder.Build();
    }
}
