﻿using System.Collections.Generic;

public class PlayerScriptableObjectThreeSameFrontCardsHandBuilder
{
    public List<CardScriptableObject> ThreeSameFrontHand()
    {
        CardScriptableObject eightOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        eightOfHearts.NumberSetterForTests(8);
        eightOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject eightOfSpades = CardScriptableObject.CreateInstance<CardScriptableObject>();
        eightOfSpades.NumberSetterForTests(8);
        eightOfSpades.CardSuitSetterForTests(CardSuitsEnum.Spades);

        CardScriptableObject eightOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        eightOfDiamonds.NumberSetterForTests(8);
        eightOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject twoOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfDiamonds.NumberSetterForTests(2);
        twoOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject fiveOfClubs = CardScriptableObject.CreateInstance<CardScriptableObject>();
        fiveOfClubs.NumberSetterForTests(5);
        fiveOfClubs.CardSuitSetterForTests(CardSuitsEnum.Clubs);

        return new List<CardScriptableObject>()
        {
            twoOfDiamonds, eightOfDiamonds, fiveOfClubs, eightOfSpades,  eightOfHearts
        };
    }

    public PlayerScriptableObject Build()
    {
        PlayerScriptableObject properties = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        properties.m_PlayerHand = ThreeSameFrontHand();

        return properties;
    }

    public static implicit operator PlayerScriptableObject(PlayerScriptableObjectThreeSameFrontCardsHandBuilder builder)
    {
        return builder.Build();
    }
}
