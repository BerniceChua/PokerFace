﻿using System.Collections.Generic;

public class PlayerScriptableObjectOnePairOf10HandBuilder
{
    public List<CardScriptableObject> OnePairOf10Hand()
    {
        CardScriptableObject eightOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        eightOfHearts.NumberSetterForTests(8);
        eightOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject tenOfSpades = CardScriptableObject.CreateInstance<CardScriptableObject>();
        tenOfSpades.NumberSetterForTests(10);
        tenOfSpades.CardSuitSetterForTests(CardSuitsEnum.Spades);

        CardScriptableObject tenOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        tenOfDiamonds.NumberSetterForTests(10);
        tenOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject twoOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfDiamonds.NumberSetterForTests(2);
        twoOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject fiveOfClubs = CardScriptableObject.CreateInstance<CardScriptableObject>();
        fiveOfClubs.NumberSetterForTests(5);
        fiveOfClubs.CardSuitSetterForTests(CardSuitsEnum.Clubs);

        return new List<CardScriptableObject>()
        {
            twoOfDiamonds, tenOfDiamonds, fiveOfClubs, tenOfSpades,  eightOfHearts
        };
    }

    public PlayerScriptableObject Build()
    {
        PlayerScriptableObject properties = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        properties.m_PlayerHand = OnePairOf10Hand();

        return properties;
    }

    public static implicit operator PlayerScriptableObject(PlayerScriptableObjectOnePairOf10HandBuilder builder)
    {
        return builder.Build();
    }
}
