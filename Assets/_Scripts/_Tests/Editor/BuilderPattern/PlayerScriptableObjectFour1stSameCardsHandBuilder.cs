﻿using System.Collections.Generic;

public class PlayerScriptableObjectFourSameCardsHandBuilder
{
    public List<CardScriptableObject> FourSameHand()
    {
        CardScriptableObject eightOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        eightOfHearts.NumberSetterForTests(8);
        eightOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject eightOfSpades = CardScriptableObject.CreateInstance<CardScriptableObject>();
        eightOfSpades.NumberSetterForTests(8);
        eightOfSpades.CardSuitSetterForTests(CardSuitsEnum.Spades);

        CardScriptableObject eightOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        eightOfDiamonds.NumberSetterForTests(8);
        eightOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject twoOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfDiamonds.NumberSetterForTests(2);
        twoOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject eightOfClubs = CardScriptableObject.CreateInstance<CardScriptableObject>();
        eightOfClubs.NumberSetterForTests(8);
        eightOfClubs.CardSuitSetterForTests(CardSuitsEnum.Clubs);

        return new List<CardScriptableObject>()
        {
            twoOfDiamonds, eightOfDiamonds, eightOfClubs, eightOfSpades,  eightOfHearts
        };
    }

    public PlayerScriptableObject Build()
    {
        PlayerScriptableObject properties = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        properties.m_PlayerHand = FourSameHand();

        return properties;
    }

    public static implicit operator PlayerScriptableObject(PlayerScriptableObjectFourSameCardsHandBuilder builder)
    {
        return builder.Build();
    }
}
