﻿using System.Collections.Generic;

public class PlayerScriptableObjectFourLastSameCardsHandBuilder
{
    public List<CardScriptableObject> FourLastSameHand()
    {
        CardScriptableObject twoOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfHearts.NumberSetterForTests(2);
        twoOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject twoOfSpades = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfSpades.NumberSetterForTests(2);
        twoOfSpades.CardSuitSetterForTests(CardSuitsEnum.Spades);

        CardScriptableObject eightOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        eightOfDiamonds.NumberSetterForTests(8);
        eightOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject twoOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfDiamonds.NumberSetterForTests(2);
        twoOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject twoOfClubs = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfClubs.NumberSetterForTests(2);
        twoOfClubs.CardSuitSetterForTests(CardSuitsEnum.Clubs);

        return new List<CardScriptableObject>()
        {
            twoOfDiamonds, eightOfDiamonds, twoOfClubs, twoOfSpades,  twoOfHearts
        };
    }

    public PlayerScriptableObject Build()
    {
        PlayerScriptableObject properties = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        properties.m_PlayerHand = FourLastSameHand();

        return properties;
    }

    public static implicit operator PlayerScriptableObject(PlayerScriptableObjectFourLastSameCardsHandBuilder builder)
    {
        return builder.Build();
    }
}
