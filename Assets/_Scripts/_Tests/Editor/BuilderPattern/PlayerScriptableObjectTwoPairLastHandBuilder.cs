﻿using System.Collections.Generic;

public class PlayerScriptableObjectTwoPairLastHandBuilder
{
    public List<CardScriptableObject> TwoLastSameHand()
    {
        CardScriptableObject eightOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        eightOfHearts.NumberSetterForTests(8);
        eightOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject twoOfSpades = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfSpades.NumberSetterForTests(2);
        twoOfSpades.CardSuitSetterForTests(CardSuitsEnum.Spades);

        CardScriptableObject fiveOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        fiveOfDiamonds.NumberSetterForTests(5);
        fiveOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject twoOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfDiamonds.NumberSetterForTests(2);
        twoOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject fiveOfClubs = CardScriptableObject.CreateInstance<CardScriptableObject>();
        fiveOfClubs.NumberSetterForTests(5);
        fiveOfClubs.CardSuitSetterForTests(CardSuitsEnum.Clubs);

        return new List<CardScriptableObject>()
        {
            twoOfDiamonds, fiveOfDiamonds, fiveOfClubs, twoOfSpades,  eightOfHearts
        };
    }

    public PlayerScriptableObject Build()
    {
        PlayerScriptableObject properties = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        properties.m_PlayerHand = TwoLastSameHand();

        return properties;
    }

    public static implicit operator PlayerScriptableObject(PlayerScriptableObjectTwoPairLastHandBuilder builder)
    {
        return builder.Build();
    }
}
