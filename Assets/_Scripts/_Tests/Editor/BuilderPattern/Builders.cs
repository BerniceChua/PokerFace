﻿public static class A
{
    public static PlayerScriptableObjectHighCardNoAceHand1Builder HighCardNoAceHand1Builder => new PlayerScriptableObjectHighCardNoAceHand1Builder();

    public static PlayerScriptableObjectFlushHandBuilder AllSameSuitsHand => new PlayerScriptableObjectFlushHandBuilder();

    public static PlayerScriptableObjectBroadwayStraightHandBuilder BroadwayStraightHand => new PlayerScriptableObjectBroadwayStraightHandBuilder();

    public static PlayerScriptableObjectBabyStraightHandBuilder BabyStraightHand => new PlayerScriptableObjectBabyStraightHandBuilder();

    public static PlayerScriptableObjectNonSpecialStraight1HandBuilder NonSpecialStraight1Hand => new PlayerScriptableObjectNonSpecialStraight1HandBuilder();

    public static PlayerScriptableObjectNonSpecialStraight2HandBuilder NonSpecialStraight2Hand => new PlayerScriptableObjectNonSpecialStraight2HandBuilder();

    public static PlayerScriptableObjectSequencialExceptLastCardHandBuilder SequencialExceptLastCardHand => new PlayerScriptableObjectSequencialExceptLastCardHandBuilder();

    public static PlayerScriptableObjectNonSpecialStraightFlushHandBuilder NonSpecialStraightFlushHand => new PlayerScriptableObjectNonSpecialStraightFlushHandBuilder();

    public static PlayerScriptableObjectBroadwayStraightFlushHandBuilder BroadwayStraightFlushHand => new PlayerScriptableObjectBroadwayStraightFlushHandBuilder();

    public static PlayerScriptableObjectBabyStraightFlushHandBuilder BabyStraightFlushHand => new PlayerScriptableObjectBabyStraightFlushHandBuilder();

    public static PlayerScriptableObjectFourSameCardsHandBuilder FourSameCardsHand => new PlayerScriptableObjectFourSameCardsHandBuilder();

    public static PlayerScriptableObjectFourLastSameCardsHandBuilder FourLastSameCardsHandBuilder => new PlayerScriptableObjectFourLastSameCardsHandBuilder();

    public static PlayerScriptableObjectFullHouseHandBuilder FullHouseHand => new PlayerScriptableObjectFullHouseHandBuilder();

    public static PlayerScriptableObjectFullHouseLast3HandBuilder FullHouseLast3HandBuilder => new PlayerScriptableObjectFullHouseLast3HandBuilder();

    public static PlayerScriptableObjectThreeSameFrontCardsHandBuilder ThreeSameFrontCardsHandBuilder => new PlayerScriptableObjectThreeSameFrontCardsHandBuilder();

    public static PlayerScriptableObjectThreeSameMiddleCardsHandBuilder ThreeSameMiddleCardsHandBuilder => new PlayerScriptableObjectThreeSameMiddleCardsHandBuilder();

    public static PlayerScriptableObjectTwoPairFrontHandBuilder TwoPairFrontHand => new PlayerScriptableObjectTwoPairFrontHandBuilder();

    public static PlayerScriptableObjectTwoPairLastHandBuilder TwoPairLastHand => new PlayerScriptableObjectTwoPairLastHandBuilder();

    public static PlayerScriptableObjectTwoPairSplitHandBuilder TwoPairSplitHand => new PlayerScriptableObjectTwoPairSplitHandBuilder();

    public static PlayerScriptableObjectOnePairHandBuilder OnePairHandBuilder => new PlayerScriptableObjectOnePairHandBuilder();

    public static PlayerScriptableObjectOnePairOf5HandBuilder OnePairOf5HandBuilder => new PlayerScriptableObjectOnePairOf5HandBuilder();

    public static PlayerScriptableObjectOnePairOf2Hand1stBuilder OnePairOf2Hand1stBuilder => new PlayerScriptableObjectOnePairOf2Hand1stBuilder();

    public static PlayerScriptableObjectOnePairOf2Hand2ndBuilder OnePairOf2Hand2ndBuilder => new PlayerScriptableObjectOnePairOf2Hand2ndBuilder();

    public static PlayerScriptableObjectOnePairOf8HandBuilder OnePairOf8HandBuilder => new PlayerScriptableObjectOnePairOf8HandBuilder();

    public static PlayerScriptableObjectOnePairOf10HandBuilder OnePairOf10HandBuilder => new PlayerScriptableObjectOnePairOf10HandBuilder();

    public static GameManagerScriptableObjectFullDeckOfCardsBuilder FullDeckOfCardsBuilder => new GameManagerScriptableObjectFullDeckOfCardsBuilder();

    public static PlayerScriptableObjectOriginallyEmptyHandBuilder OriginallyEmptyHandBuilder => new PlayerScriptableObjectOriginallyEmptyHandBuilder();

    public static PlayerScriptableObjectHighCardNoAceHand2Builder HighCardNoAceHand2Builder => new PlayerScriptableObjectHighCardNoAceHand2Builder();

    public static PlayerScriptableObjectHighCardsWithAceHand1Builder HighCardsWithAceHand1Builder => new PlayerScriptableObjectHighCardsWithAceHand1Builder();

    public static PlayerScriptableObjectHighCardsWithAceHand2Builder HighCardsWithAceHand2Builder => new PlayerScriptableObjectHighCardsWithAceHand2Builder();

}

public static class An
{

}
