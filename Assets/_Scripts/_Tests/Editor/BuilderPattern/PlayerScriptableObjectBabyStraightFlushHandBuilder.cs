﻿using System.Collections.Generic;

public class PlayerScriptableObjectBabyStraightFlushHandBuilder
{
    public List<CardScriptableObject> BabyStraightFlushHand()
    {
        CardScriptableObject aceOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        aceOfHearts.NumberSetterForTests(1);
        aceOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject fourOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        fourOfHearts.NumberSetterForTests(4);
        fourOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject fiveOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        fiveOfHearts.NumberSetterForTests(5);
        fiveOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject twoOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfHearts.NumberSetterForTests(2);
        twoOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject threeOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        threeOfHearts.NumberSetterForTests(3);
        threeOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        return new List<CardScriptableObject>()
        {
            twoOfHearts, fiveOfHearts, threeOfHearts, fourOfHearts,  aceOfHearts
        };
    }

    public PlayerScriptableObject Build()
    {
        PlayerScriptableObject properties = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        properties.m_PlayerHand = BabyStraightFlushHand();

        return properties;
    }

    public static implicit operator PlayerScriptableObject(PlayerScriptableObjectBabyStraightFlushHandBuilder builder)
    {
        return builder.Build();
    }
}
