﻿using System.Collections.Generic;

public class PlayerScriptableObjectTwoPairSplitHandBuilder
{
    public List<CardScriptableObject> TwoSplitSameHand()
    {
        CardScriptableObject eightOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        eightOfHearts.NumberSetterForTests(8);
        eightOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject twoOfSpades = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfSpades.NumberSetterForTests(2);
        twoOfSpades.CardSuitSetterForTests(CardSuitsEnum.Spades);

        CardScriptableObject fiveOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        fiveOfDiamonds.NumberSetterForTests(5);
        fiveOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject twoOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfDiamonds.NumberSetterForTests(2);
        twoOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject eightOfClubs = CardScriptableObject.CreateInstance<CardScriptableObject>();
        eightOfClubs.NumberSetterForTests(8);
        eightOfClubs.CardSuitSetterForTests(CardSuitsEnum.Clubs);

        return new List<CardScriptableObject>()
        {
            twoOfDiamonds, fiveOfDiamonds, eightOfClubs, twoOfSpades,  eightOfHearts
        };
    }

    public PlayerScriptableObject Build()
    {
        PlayerScriptableObject properties = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        properties.m_PlayerHand = TwoSplitSameHand();

        return properties;
    }

    public static implicit operator PlayerScriptableObject(PlayerScriptableObjectTwoPairSplitHandBuilder builder)
    {
        return builder.Build();
    }
}
