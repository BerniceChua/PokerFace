﻿using System.Collections.Generic;

public class PlayerScriptableObjectHighCardNoAceHand1Builder
{
    public List<CardScriptableObject> HighCardNoAceHand1()
    {
        CardScriptableObject sevenOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        sevenOfHearts.NumberSetterForTests(7);
        sevenOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject queenOfSpades = CardScriptableObject.CreateInstance<CardScriptableObject>();
        queenOfSpades.NumberSetterForTests(12);
        queenOfSpades.CardSuitSetterForTests(CardSuitsEnum.Spades);

        CardScriptableObject eightOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        eightOfDiamonds.NumberSetterForTests(8);
        eightOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject twoOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfDiamonds.NumberSetterForTests(2);
        twoOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject fiveOfClubs = CardScriptableObject.CreateInstance<CardScriptableObject>();
        fiveOfClubs.NumberSetterForTests(5);
        fiveOfClubs.CardSuitSetterForTests(CardSuitsEnum.Clubs);

        return new List<CardScriptableObject>()
        {
            twoOfDiamonds, eightOfDiamonds, fiveOfClubs, queenOfSpades,  sevenOfHearts
        };
    }

    public PlayerScriptableObject Build()
    {
        PlayerScriptableObject properties = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        properties.m_PlayerHand = HighCardNoAceHand1();

        return properties;
    }

    public static implicit operator PlayerScriptableObject(PlayerScriptableObjectHighCardNoAceHand1Builder builder)
    {
        return builder.Build();
    }
}
