﻿using System.Collections.Generic;

public class PlayerScriptableObjectNonSpecialStraight2HandBuilder
{
    public List<CardScriptableObject> NonSpecialStraight2()
    {
        CardScriptableObject fourOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        fourOfHearts.NumberSetterForTests(4);
        fourOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject fiveOfSpades = CardScriptableObject.CreateInstance<CardScriptableObject>();
        fiveOfSpades.NumberSetterForTests(5);
        fiveOfSpades.CardSuitSetterForTests(CardSuitsEnum.Spades);

        CardScriptableObject threeOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        threeOfDiamonds.NumberSetterForTests(3);
        threeOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject sixOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        sixOfDiamonds.NumberSetterForTests(6);
        sixOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject twoOfClubs = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfClubs.NumberSetterForTests(2);
        twoOfClubs.CardSuitSetterForTests(CardSuitsEnum.Clubs);

        return new List<CardScriptableObject>()
        {
            sixOfDiamonds, threeOfDiamonds, twoOfClubs, fiveOfSpades,  fourOfHearts
        };
    }

    public PlayerScriptableObject Build()
    {
        PlayerScriptableObject properties = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        properties.m_PlayerHand = NonSpecialStraight2();

        return properties;
    }

    public static implicit operator PlayerScriptableObject(PlayerScriptableObjectNonSpecialStraight2HandBuilder builder)
    {
        return builder.Build();
    }
}
