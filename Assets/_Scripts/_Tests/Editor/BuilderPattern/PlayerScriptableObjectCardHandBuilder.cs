﻿using System.Collections.Generic;

public class PlayerScriptableObjectFlushHandBuilder
{
    public List<CardScriptableObject> FlushHand()
    {
        CardScriptableObject sevenOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        sevenOfHearts.NumberSetterForTests(7);
        sevenOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject queenOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        queenOfHearts.NumberSetterForTests(12);
        queenOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject eightOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        eightOfHearts.NumberSetterForTests(8);
        eightOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject twoOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfHearts.NumberSetterForTests(2);
        twoOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject fiveOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        fiveOfHearts.NumberSetterForTests(5);
        fiveOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        return new List<CardScriptableObject>()
        {
            twoOfHearts, eightOfHearts, fiveOfHearts, queenOfHearts,  sevenOfHearts
        };
    }

    public PlayerScriptableObject Build()
    {
        PlayerScriptableObject properties = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        properties.m_PlayerHand = FlushHand();

        return properties;
    }

    public static implicit operator PlayerScriptableObject(PlayerScriptableObjectFlushHandBuilder builder)
    {
        return builder.Build();
    }
}
