﻿using System.Collections.Generic;

public class PlayerScriptableObjectHighCardsWithAceHand2Builder
{
    public List<CardScriptableObject> RandomCardsWithAceHand()
    {
        CardScriptableObject aceOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        aceOfHearts.NumberSetterForTests(1);
        aceOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject queenOfSpades = CardScriptableObject.CreateInstance<CardScriptableObject>();
        queenOfSpades.NumberSetterForTests(12);
        queenOfSpades.CardSuitSetterForTests(CardSuitsEnum.Spades);

        CardScriptableObject eightOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        eightOfDiamonds.NumberSetterForTests(8);
        eightOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject twoOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfDiamonds.NumberSetterForTests(2);
        twoOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject fiveOfClubs = CardScriptableObject.CreateInstance<CardScriptableObject>();
        fiveOfClubs.NumberSetterForTests(5);
        fiveOfClubs.CardSuitSetterForTests(CardSuitsEnum.Clubs);

        return new List<CardScriptableObject>()
        {
            twoOfDiamonds, eightOfDiamonds, fiveOfClubs, queenOfSpades,  aceOfHearts
        };
    }

    public PlayerScriptableObject Build()
    {
        PlayerScriptableObject properties = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        properties.m_PlayerHand = RandomCardsWithAceHand();

        return properties;
    }

    public static implicit operator PlayerScriptableObject(PlayerScriptableObjectHighCardsWithAceHand2Builder builder)
    {
        return builder.Build();
    }
}
