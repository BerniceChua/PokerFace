﻿using System.Collections.Generic;

public class PlayerScriptableObjectBroadwayStraightHandBuilder
{
    public List<CardScriptableObject> BroadwayStraightHand()
    {
        CardScriptableObject aceOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        aceOfHearts.NumberSetterForTests(1);
        aceOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject queenOfSpades = CardScriptableObject.CreateInstance<CardScriptableObject>();
        queenOfSpades.NumberSetterForTests(12);
        queenOfSpades.CardSuitSetterForTests(CardSuitsEnum.Spades);

        CardScriptableObject jackOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        jackOfDiamonds.NumberSetterForTests(11);
        jackOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject tenOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        tenOfDiamonds.NumberSetterForTests(10);
        tenOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject kingOfClubs = CardScriptableObject.CreateInstance<CardScriptableObject>();
        kingOfClubs.NumberSetterForTests(13);
        kingOfClubs.CardSuitSetterForTests(CardSuitsEnum.Clubs);

        return new List<CardScriptableObject>()
        {
            tenOfDiamonds, jackOfDiamonds, kingOfClubs, queenOfSpades,  aceOfHearts
        };
    }

    public PlayerScriptableObject Build()
    {
        PlayerScriptableObject properties = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        properties.m_PlayerHand = BroadwayStraightHand();

        return properties;
    }

    public static implicit operator PlayerScriptableObject(PlayerScriptableObjectBroadwayStraightHandBuilder builder)
    {
        return builder.Build();
    }
}
