﻿using System.Collections.Generic;
using System;

public class GameManagerScriptableObjectFullDeckOfCardsBuilder
{
    private List<CardScriptableObject> m_fullDeck = new List<CardScriptableObject>();

    public List<CardScriptableObject> GenerateAllCards()
    {
        //foreach (var suit in Enum.GetValues(typeof(CardSuitsEnum)))
        //{
        //    for (int i = 1; i < 13; i++)
        //    {
        //        CardScriptableObject card = CardScriptableObject.CreateInstance("CardScriptableObject") as CardScriptableObject;
        //        card.NumberSetterForTests(suit.ToString());
        //    }
        //}

        for (int i = 0; i < 13; i++)
        {
            m_fullDeck.Add(CardScriptableObject.CreateInstance<CardScriptableObject>() );
            m_fullDeck[i].NumberSetterForTests(i + 1);
            m_fullDeck[i].CardSuitSetterForTests(CardSuitsEnum.Clubs);
        }

        for (int i = 13; i < 26; i++)
        {
            m_fullDeck.Add(CardScriptableObject.CreateInstance<CardScriptableObject>() );
            m_fullDeck[i].NumberSetterForTests( (i-13) + 1);
            m_fullDeck[i].CardSuitSetterForTests(CardSuitsEnum.Diamonds);
        }

        for (int i = 26; i < 39; i++)
        {
            m_fullDeck.Add(CardScriptableObject.CreateInstance<CardScriptableObject>() );
            m_fullDeck[i].NumberSetterForTests( (i-26) + 1);
            m_fullDeck[i].CardSuitSetterForTests(CardSuitsEnum.Hearts);
        }

        for (int i = 39; i < 52; i++)
        {
            m_fullDeck.Add(CardScriptableObject.CreateInstance<CardScriptableObject>() );
            m_fullDeck[i].NumberSetterForTests( (i-39) + 1);
            m_fullDeck[i].CardSuitSetterForTests(CardSuitsEnum.Spades);
        }

        return m_fullDeck;
    }

    public List<CardScriptableObject> Build()
    {
        List<CardScriptableObject> properties = GenerateAllCards();

        return properties;
    }

    public static implicit operator List<CardScriptableObject>(GameManagerScriptableObjectFullDeckOfCardsBuilder builder)
    {
        return builder.Build();
    }
}
