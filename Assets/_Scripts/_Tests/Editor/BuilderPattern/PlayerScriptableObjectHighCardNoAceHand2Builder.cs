﻿using System.Collections.Generic;

public class PlayerScriptableObjectHighCardNoAceHand2Builder
{
    public List<CardScriptableObject> HighCardNoAceHand2()
    {
        CardScriptableObject sevenOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        sevenOfHearts.NumberSetterForTests(7);
        sevenOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject queenOfSpades = CardScriptableObject.CreateInstance<CardScriptableObject>();
        queenOfSpades.NumberSetterForTests(12);
        queenOfSpades.CardSuitSetterForTests(CardSuitsEnum.Spades);

        CardScriptableObject eightOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        eightOfDiamonds.NumberSetterForTests(8);
        eightOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject threeOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        threeOfDiamonds.NumberSetterForTests(3);
        threeOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject fiveOfClubs = CardScriptableObject.CreateInstance<CardScriptableObject>();
        fiveOfClubs.NumberSetterForTests(5);
        fiveOfClubs.CardSuitSetterForTests(CardSuitsEnum.Clubs);

        return new List<CardScriptableObject>()
        {
            threeOfDiamonds, eightOfDiamonds, fiveOfClubs, queenOfSpades,  sevenOfHearts
        };
    }

    public PlayerScriptableObject Build()
    {
        PlayerScriptableObject properties = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        properties.m_PlayerHand = HighCardNoAceHand2();

        return properties;
    }

    public static implicit operator PlayerScriptableObject(PlayerScriptableObjectHighCardNoAceHand2Builder builder)
    {
        return builder.Build();
    }
}
