﻿using System.Collections.Generic;

public class PlayerScriptableObjectBabyStraightHandBuilder
{
    public List<CardScriptableObject> BabyStraightHand()
    {
        CardScriptableObject aceOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        aceOfHearts.NumberSetterForTests(1);
        aceOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject fourOfSpades = CardScriptableObject.CreateInstance<CardScriptableObject>();
        fourOfSpades.NumberSetterForTests(4);
        fourOfSpades.CardSuitSetterForTests(CardSuitsEnum.Spades);

        CardScriptableObject fiveOfDiamonds = CardScriptableObject.CreateInstance("CardScriptableObject") as CardScriptableObject;
        fiveOfDiamonds.NumberSetterForTests(5);
        fiveOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject twoOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfDiamonds.NumberSetterForTests(2);
        twoOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject threeOfClubs = CardScriptableObject.CreateInstance<CardScriptableObject>();
        threeOfClubs.NumberSetterForTests(3);
        threeOfClubs.CardSuitSetterForTests(CardSuitsEnum.Clubs);

        return new List<CardScriptableObject>()
        {
            twoOfDiamonds, fiveOfDiamonds, threeOfClubs, fourOfSpades,  aceOfHearts
        };
    }

    public PlayerScriptableObject Build()
    {
        PlayerScriptableObject properties = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        properties.m_PlayerHand = BabyStraightHand();

        return properties;
    }

    public static implicit operator PlayerScriptableObject(PlayerScriptableObjectBabyStraightHandBuilder builder)
    {
        return builder.Build();
    }
}
