﻿using System.Collections.Generic;

public class PlayerScriptableObjectSequencialExceptLastCardHandBuilder
{
    public List<CardScriptableObject> SequencialExceptLastCard()
    {
        CardScriptableObject fiveOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        fiveOfHearts.NumberSetterForTests(5);
        fiveOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject sixOfSpades = CardScriptableObject.CreateInstance<CardScriptableObject>();
        sixOfSpades.NumberSetterForTests(6);
        sixOfSpades.CardSuitSetterForTests(CardSuitsEnum.Spades);

        CardScriptableObject fourOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        fourOfDiamonds.NumberSetterForTests(4);
        fourOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject sevenOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        sevenOfDiamonds.NumberSetterForTests(7);
        sevenOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject twoOfClubs = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfClubs.NumberSetterForTests(2);
        twoOfClubs.CardSuitSetterForTests(CardSuitsEnum.Clubs);

        return new List<CardScriptableObject>()
        {
            sevenOfDiamonds, fourOfDiamonds, twoOfClubs, sixOfSpades,  fiveOfHearts
        };
    }

    public PlayerScriptableObject Build()
    {
        PlayerScriptableObject properties = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        properties.m_PlayerHand = SequencialExceptLastCard();

        return properties;
    }

    public static implicit operator PlayerScriptableObject(PlayerScriptableObjectSequencialExceptLastCardHandBuilder builder)
    {
        return builder.Build();
    }
}
