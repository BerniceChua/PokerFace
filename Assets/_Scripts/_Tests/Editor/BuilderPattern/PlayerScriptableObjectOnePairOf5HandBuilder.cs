﻿using System.Collections.Generic;

public class PlayerScriptableObjectOnePairOf5HandBuilder
{
    public List<CardScriptableObject> OnePairOf5Hand()
    {
        CardScriptableObject fiveOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        fiveOfHearts.NumberSetterForTests(5);
        fiveOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject queenOfSpades = CardScriptableObject.CreateInstance<CardScriptableObject>();
        queenOfSpades.NumberSetterForTests(12);
        queenOfSpades.CardSuitSetterForTests(CardSuitsEnum.Spades);

        CardScriptableObject eightOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        eightOfDiamonds.NumberSetterForTests(8);
        eightOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject jackOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        jackOfDiamonds.NumberSetterForTests(11);
        jackOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject fiveOfClubs = CardScriptableObject.CreateInstance<CardScriptableObject>();
        fiveOfClubs.NumberSetterForTests(5);
        fiveOfClubs.CardSuitSetterForTests(CardSuitsEnum.Clubs);

        return new List<CardScriptableObject>()
        {
            jackOfDiamonds, eightOfDiamonds, fiveOfClubs, queenOfSpades,  fiveOfHearts
        };
    }

    public PlayerScriptableObject Build()
    {
        PlayerScriptableObject properties = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        properties.m_PlayerHand = OnePairOf5Hand();

        return properties;
    }

    public static implicit operator PlayerScriptableObject(PlayerScriptableObjectOnePairOf5HandBuilder builder)
    {
        return builder.Build();
    }
}
