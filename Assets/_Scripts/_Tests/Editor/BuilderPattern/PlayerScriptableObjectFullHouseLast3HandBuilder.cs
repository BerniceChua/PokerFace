﻿using System.Collections.Generic;

public class PlayerScriptableObjectFullHouseLast3HandBuilder
{
    public List<CardScriptableObject> FullHouseLast3Hand()
    {
        CardScriptableObject eightOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        eightOfHearts.NumberSetterForTests(8);
        eightOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject twoOfSpades = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfSpades.NumberSetterForTests(2);
        twoOfSpades.CardSuitSetterForTests(CardSuitsEnum.Spades);

        CardScriptableObject eightOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        eightOfDiamonds.NumberSetterForTests(8);
        eightOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject twoOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfDiamonds.NumberSetterForTests(2);
        twoOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject twoOfClubs = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfClubs.NumberSetterForTests(2);
        twoOfClubs.CardSuitSetterForTests(CardSuitsEnum.Clubs);

        return new List<CardScriptableObject>()
        {
            twoOfDiamonds, eightOfDiamonds, twoOfClubs, twoOfSpades,  eightOfHearts
        };
    }

    public PlayerScriptableObject Build()
    {
        PlayerScriptableObject properties = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        properties.m_PlayerHand = FullHouseLast3Hand();

        return properties;
    }

    public static implicit operator PlayerScriptableObject(PlayerScriptableObjectFullHouseLast3HandBuilder builder)
    {
        return builder.Build();
    }
}
