﻿using System.Collections.Generic;

public class PlayerScriptableObjectOnePairOf2Hand1stBuilder
{
    public List<CardScriptableObject> OnePairOf2Hand()
    {
        CardScriptableObject eightOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        eightOfHearts.NumberSetterForTests(8);
        eightOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject queenOfSpades = CardScriptableObject.CreateInstance<CardScriptableObject>();
        queenOfSpades.NumberSetterForTests(12);
        queenOfSpades.CardSuitSetterForTests(CardSuitsEnum.Spades);

        CardScriptableObject twoOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfDiamonds.NumberSetterForTests(2);
        twoOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject jackOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        jackOfDiamonds.NumberSetterForTests(11);
        jackOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject twoOfClubs = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfClubs.NumberSetterForTests(2);
        twoOfClubs.CardSuitSetterForTests(CardSuitsEnum.Clubs);

        return new List<CardScriptableObject>()
        {
            jackOfDiamonds, twoOfDiamonds, twoOfClubs, queenOfSpades,  eightOfHearts
        };
    }

    public PlayerScriptableObject Build()
    {
        PlayerScriptableObject properties = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        properties.m_PlayerHand = OnePairOf2Hand();

        return properties;
    }

    public static implicit operator PlayerScriptableObject(PlayerScriptableObjectOnePairOf2Hand1stBuilder builder)
    {
        return builder.Build();
    }
}
