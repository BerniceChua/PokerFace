﻿using System.Collections.Generic;

public class PlayerScriptableObjectHighCardsWithAceHand1Builder
{
    public List<CardScriptableObject> HighCardWithAceHand1()
    {
        CardScriptableObject sevenOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        sevenOfHearts.NumberSetterForTests(7);
        sevenOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject aceOfSpades = CardScriptableObject.CreateInstance<CardScriptableObject>();
        aceOfSpades.NumberSetterForTests(1);
        aceOfSpades.CardSuitSetterForTests(CardSuitsEnum.Spades);

        CardScriptableObject threeOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        threeOfDiamonds.NumberSetterForTests(3);
        threeOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject twoOfDiamonds = CardScriptableObject.CreateInstance<CardScriptableObject>();
        twoOfDiamonds.NumberSetterForTests(2);
        twoOfDiamonds.CardSuitSetterForTests(CardSuitsEnum.Diamonds);

        CardScriptableObject fiveOfClubs = CardScriptableObject.CreateInstance<CardScriptableObject>();
        fiveOfClubs.NumberSetterForTests(5);
        fiveOfClubs.CardSuitSetterForTests(CardSuitsEnum.Clubs);

        return new List<CardScriptableObject>()
        {
            twoOfDiamonds, threeOfDiamonds, fiveOfClubs, aceOfSpades,  sevenOfHearts
        };
    }

    public PlayerScriptableObject Build()
    {
        PlayerScriptableObject properties = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        properties.m_PlayerHand = HighCardWithAceHand1();

        return properties;
    }

    public static implicit operator PlayerScriptableObject(PlayerScriptableObjectHighCardsWithAceHand1Builder builder)
    {
        return builder.Build();
    }
}
