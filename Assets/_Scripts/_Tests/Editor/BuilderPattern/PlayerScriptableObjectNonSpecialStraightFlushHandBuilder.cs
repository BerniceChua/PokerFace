﻿using System.Collections.Generic;

public class PlayerScriptableObjectNonSpecialStraightFlushHandBuilder
{
    public List<CardScriptableObject> NonSpecialStraightFlush()
    {
        CardScriptableObject eightOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        eightOfHearts.NumberSetterForTests(8);
        eightOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject nineOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        nineOfHearts.NumberSetterForTests(9);
        nineOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject sevenOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        sevenOfHearts.NumberSetterForTests(7);
        sevenOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject tenOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        tenOfHearts.NumberSetterForTests(10);
        tenOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        CardScriptableObject sixOfHearts = CardScriptableObject.CreateInstance<CardScriptableObject>();
        sixOfHearts.NumberSetterForTests(6);
        sixOfHearts.CardSuitSetterForTests(CardSuitsEnum.Hearts);

        return new List<CardScriptableObject>()
        {
            tenOfHearts, sevenOfHearts, sixOfHearts, nineOfHearts,  eightOfHearts
        };
    }

    public PlayerScriptableObject Build()
    {
        PlayerScriptableObject properties = PlayerScriptableObject.CreateInstance<PlayerScriptableObject>();
        properties.m_PlayerHand = NonSpecialStraightFlush();

        return properties;
    }

    public static implicit operator PlayerScriptableObject(PlayerScriptableObjectNonSpecialStraightFlushHandBuilder builder)
    {
        return builder.Build();
    }
}
