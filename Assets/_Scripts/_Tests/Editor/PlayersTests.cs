﻿using NUnit.Framework;
using NSubstitute;
using System.Collections;
using System.Collections.Generic;
using FluentAssertions;

public class PlayersTests
{
    [Test]
    public void TestThatEachPlayerStartsWithNoCards()
    {
        PlayerScriptableObject thisPlayer = PlayerScriptableObject.CreateInstance("PlayerScriptableObject") as PlayerScriptableObject;
        int cardCount = thisPlayer.m_PlayerHand.Count;
        Assert.Zero(cardCount);
    }

    [Test]
    public void TestThatEachPlayerGets5Cards()
    {
        #region ASSIGN / ARRANGE
        GameLoopManager thisGameManager = new GameLoopManager();
        IGameLoopManagerBehaviour gameManagerBehaviour = Substitute.For<IGameLoopManagerBehaviour>();

        GameManagerScriptableObjectFullDeckOfCardsBuilder fullDeckOfCards = A.FullDeckOfCardsBuilder;
        List<CardScriptableObject> fullDeck = fullDeckOfCards;

        Player thisPlayer = new Player();
        IPlayerBehaviour player1 = Substitute.For<IPlayerBehaviour>();
        PlayerScriptableObject playerSO = PlayerScriptableObject.CreateInstance("PlayerScriptableObject") as PlayerScriptableObject;

        thisGameManager.Shuffle(fullDeck);
        #endregion

        #region ACT
        thisGameManager.DistributeCards(fullDeck, playerSO, 0);
        int handCount = playerSO.m_PlayerHand.Count;
        #endregion

        #region ASSERT
        //Assert.AreEqual(5, cardCount);
        handCount.Should().Be(5);
        #endregion
    }

    //[Test]
    //public void TestThatEachPlayerGetsDifferentSetsOfCards()
    //{
    //    GameManager thisGameManager = new GameManager();
    //    IGameManagerBehaviour gameManagerBehaviour = Substitute.For<IGameManagerBehaviour>();
    //    List<CardScriptableObject> allCards = gameManagerBehaviour.AllListOfCards;

    //    Player thisPlayer = new Player();
    //    int cardCount = thisPlayer.m_CardsInHandList.Count;
    //    thisPlayer.AddCards(allCards, thisPlayer.m_CardsInHandList);

    //    Assert.IsTrue(;
    //}
}
