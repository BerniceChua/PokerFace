﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameLoopManagerBehaviour : MonoBehaviour, IGameLoopManagerBehaviour
{
    public GameLoopManager m_gameManager = new GameLoopManager();

    public IntReference m_MaxPlayers;
    public IntReference m_MinPlayers;
    /// <summary>
    /// Number of players, between 2 to 7 inclusive
    /// </summary>
    [Range(2, 7)]
    //[Range(m_gameManager.m_MinPlayers.ConstantValue, m_gameManager.m_MaxPlayers.ConstantValue)]
    [SerializeField] private IntReference m_numberOfPlayers;

    [SerializeField] private bool m_bAllowJoker;

    [SerializeField] private List<CardScriptableObject> m_allListOfCards = new List<CardScriptableObject>();
#if UNITY_EDITOR
    public void AllCards(List<CardScriptableObject> allListOfCards)
    {
        m_allListOfCards = allListOfCards;
    }
#endif
    public List<CardScriptableObject> AllListOfCards => m_allListOfCards;

    public PlayerBehaviour Player1;
    public PlayerBehaviour Player2;

    [SerializeField] private PlayerBehaviour[] m_listOfPlayers;
    public PlayerBehaviour[] ListOfPlayers
    {
        get { return m_listOfPlayers; }
    }
    public Player[] m_PlayerArray;

    public List<CardScriptableObject> Player1Hand;
    public List<CardScriptableObject> Player2Hand;

    private PlayerScriptableObject m_player1SO;
    private PlayerScriptableObject m_player2SO;

    private List<PlayerScriptableObject> m_allPlayersList;

    public PlayerScriptableObject m_Winner;

    // Start is called before the first frame update
    void Start()
    {
        m_gameManager.m_HandSize.ConstantValue = 5;

        m_gameManager.m_MaxPlayers.ConstantValue = 7;
        m_gameManager.m_MinPlayers.ConstantValue = 2;

        GetListOfPlayers();

        //Debug.Log("AllListOfCards.Count = " + AllListOfCards.Count);

        m_player1SO = Player1.m_Player.m_PlayerScriptableObject;
        m_player2SO = Player2.m_Player.m_PlayerScriptableObject;

        m_allPlayersList = new List<PlayerScriptableObject>()
        {
            m_player1SO, m_player2SO
        };

        Player1Hand = m_player1SO.m_PlayerHand;
        Player2Hand = m_player2SO.m_PlayerHand;
    }

    //// Update is called once per frame
    //void Update()
    //{

    //}

    private void GetListOfPlayers()
    {
        m_listOfPlayers = new PlayerBehaviour[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            m_listOfPlayers[i] = transform.GetChild(i).GetComponent<PlayerBehaviour>();
        }
    }

    //private void IsJokerAllowed()
    //{
    //    //if (m_bAllowJoker == true)
    //    //{
    //    //    foreach (CardScriptableObject card in m_allListOfCards)
    //    //    {
    //    //        card.bAllowJoker = true;
    //    //        card.GetMaxNumber();
    //    //    }
    //    //}
    //    //else
    //    //{
    //    //    foreach (CardScriptableObject card in m_allListOfCards)
    //    //    {
    //    //        card.bAllowJoker = false;
    //    //        card.GetMaxNumber();
    //    //    }
    //    //}

    //    if (m_bAllowJoker == true && m_allListOfCards.Count == 52)
    //    {
    //        m_allListOfCards.Add();
    //    }
    //    else
    //    {
    //        m_allListOfCards.Remove();
    //    }

    //    Debug.Log("m_bAllowJoker = " + m_bAllowJoker);
    //}

    #region Logic Behind UI Buttons
    public void Shuffle()
    {
        //foreach (IPlayerBehaviour player in m_listOfPlayers)
        //{
        //    player.ResetPlayerHand(m_listOfPlayers.);
        //}

        m_gameManager.Shuffle(m_allListOfCards);
    }

    public void DistributeCards()
    {
        int position = 0;
        //foreach (Player thisPlayer in m_PlayerArray)
        //{
        //    Debug.Log("player.name = " + thisPlayer.ToString());
        //    //m_gameManager.DistributeCards(m_allListOfCards, player);
        //    m_gameManager.DistributeCards(m_allListOfCards, thisPlayer, position);

        //    //position += 5;
        //    position += m_gameManager.m_HandSize.ConstantValue;
        //}

        //foreach (IPlayerBehaviour thisPlayer in m_listOfPlayers)
        //foreach (PlayerBehaviour thisPlayer in m_listOfPlayers)
        foreach (PlayerScriptableObject thisPlayer in m_allPlayersList)
        {
            Debug.Log("player.name = " + thisPlayer.name);
            //m_gameManager.DistributeCards(m_allListOfCards, player);
            m_gameManager.DistributeCards(m_allListOfCards, thisPlayer, position);

            //position += 5;
            position += m_gameManager.m_HandSize.ConstantValue;
        }

        Player1Hand = m_player1SO.m_PlayerHand;
        Player2Hand = m_player2SO.m_PlayerHand;
        ///// randomize between players who goes first
        //int whoGoes1st = Random.Range(0, 1);

        //if (whoGoes1st == 0)
        //{
        //    Player1.AddCardsToHand(m_allListOfCards, Player1.m_Player.m_CardsInHandList);
        //    Player2.AddCardsToHand(m_allListOfCards, Player2.m_Player.m_CardsInHandList);
        //}
        //else
        //{
        //    Player2.AddCardsToHand(m_allListOfCards, Player2.m_Player.m_CardsInHandList);
        //    Player1.AddCardsToHand(m_allListOfCards, Player1.m_Player.m_CardsInHandList);
        //}
    }

    public void CompareHands()
    {
        m_Winner = Compare2Hands(m_player1SO, m_player2SO);

        /// This part is just for sorting in the scriptable objects themselves:
        m_player1SO.m_PlayerHand = SortByAttackPower(Player1Hand);
        m_player2SO.m_PlayerHand = SortByAttackPower(Player2Hand);

        Player1Hand = m_player1SO.m_PlayerHand;
        Player2Hand = m_player2SO.m_PlayerHand;
    }
    #endregion

    /// <summary>
    /// Compare 2 Hands:
    /// Step 1: sort each hand by descending order
    /// Step 2: identify what's in each hand
    /// Step 3: compare each hand to each other
    /// Step 4: identify the winner
    /// Step 5: return the player who won the round
    /// </summary>
    //private void Compare2Hands(PlayerScriptableObject p1, PlayerScriptableObject p2)
    public PlayerScriptableObject Compare2Hands(PlayerScriptableObject p1, PlayerScriptableObject p2)
    {
        return m_gameManager.Compare2Hands(p1, p2);
    }

    public PlayerScriptableObject Winner()
    {
        return m_Winner;
    }

    public void PlayAgainButtonLogic()
    {
        PlayAgainLogic(m_listOfPlayers);
    }

    private void PlayAgainLogic(PlayerBehaviour[] arrayOfPlayers)
    {
        m_gameManager.PlayAgainLogic(arrayOfPlayers);
    }

    //private void WhatIsInThisHand(PlayerScriptableObject thisPlayer, List<CardScriptableObject> sortedHand)
    //{
    //    foreach (var player in m_listOfPlayers)
    //    {
    //        player.m_Player.m_PlayerScriptableObject.ShowCards();
    //    }

    //    m_gameManager.WhatIsInThisHand(thisPlayer, sortedHand);
    //}

    //public PlayerScriptableObject FindWinner(List<CardScriptableObject> player1Hand, List<CardScriptableObject> player2Hand)
    //{
    //    PlayerScriptableObject winner;

    //    //if (Player1.m_PlayerHand.Contains())
    //    //{
    //    //    winner = Player1.m_PlayerHand;
    //    //}
    //    //else
    //    //{
    //    //    winner = Player2.m_PlayerHand;
    //    //}

    //    return winner;
    //}

    #region Helper Functions

    #region Sorters
    private List<CardScriptableObject> SortByAttackPower(List<CardScriptableObject> thisHand)
    {
        List<CardScriptableObject> sortByAttackPower = new List<CardScriptableObject>();

        List<CardScriptableObject> sortedHand = thisHand.OrderByDescending(o => o.AttackPower).ToList();
        foreach (var item in sortedHand)
        {
            //Debug.Log(item);
            sortByAttackPower.Add(item);
        }

        return sortByAttackPower;
    }

    private List<CardScriptableObject> SortByCardNumber(List<CardScriptableObject> thisHand)
    {
        List<CardScriptableObject> sortByCardNumber = new List<CardScriptableObject>();

        List<CardScriptableObject> sortedHand = thisHand.OrderByDescending(o => o.AttackPower).ToList();
        foreach (var item in sortedHand)
        {
            //Debug.Log(item);
            sortByCardNumber.Add(item);
        }

        return sortByCardNumber;
    }
    #endregion

    public int TieredHandTypes(string thisHand)
    {
        Dictionary<string, int> ranking = new Dictionary<string, int>
        {
            ["Five Of A Kind"] = 10,
            ["Straight Flush"] = 9,
            ["Four Of A Kind"] = 8,
            ["Full House"] = 7,
            ["Flush"] = 6,
            ["Straight"] = 5,
            ["Three Of A Kind"] = 4,
            ["Two Pair"] = 3,
            ["One Pair"] = 2,
            ["High Card / No Pair / None"] = 1,
        };

        int value = 0;
        if (ranking.TryGetValue(thisHand, out value))
        {
            return value;
        }
        else
        {
            return 0;
        }
    }

    #region Check What Kind Of Hand
    /// <summary>
    /// Flush means all cards are the same suit.
    /// </summary>
    /// <param name="suitOf1stCard"></param>
    /// <param name="thisHand"></param>
    /// <returns></returns>
    private bool CheckIfFlush(string suitOf1stCard, List<CardScriptableObject> thisHand)
    {
        foreach (CardScriptableObject card in thisHand)
        {
            if(card.Suit != suitOf1stCard)
            {
                return false;
            }
        }

        //Player1.PlayerScriptable.HandType = "Flush";
        //Debug.Log("Player1.HandType = " + Player1.PlayerScriptable.HandType);
        return true;
    }

    private bool CheckIfStraight(int attackPowerOf1stCard, List<CardScriptableObject> thisHand)
    {
        /// An ace-high straight, such as A♣ K♣ Q♦ J♠ 10♠, is called a Broadway straight
        bool bIsBroadwayStraight = false;
        /// A five-high straight, such as 5♠ 4♦ 3♦ 2♠ A♥, is called a baby straight
        bool bIsBabyStraight = false;

        /// Check if it contains an Ace
        if (attackPowerOf1stCard == 14)
        {
            Debug.Log("Check if contains Ace of 'Check If Straight'.");

            bIsBroadwayStraight = (thisHand[1].AttackPower == 13) && 
                (thisHand[2].AttackPower == 12) &&
                (thisHand[3].AttackPower == 11) &&
                (thisHand[4].AttackPower == 10);

            bIsBabyStraight = (thisHand[1].AttackPower == 5) &&
                (thisHand[2].AttackPower == 4) &&
                (thisHand[3].AttackPower == 3) &&
                (thisHand[4].AttackPower == 2);

            //thisHand.HandType = TieredHandTypes(TieredHandTypesEnum.Straight);
            Debug.Log("bIsBroadwayStraight = " + bIsBroadwayStraight);
            Debug.Log("bIsBabyStraight = " + bIsBabyStraight);
            return bIsBroadwayStraight || bIsBabyStraight;
        }
        else
        {
            Debug.Log("inside the else of Check If Straight.");

            for (int i = 1; i < thisHand.Count; i++)
            {
                Debug.Log("i = " + i);
                Debug.Log("thisHand[i-1].AttackPower = " + thisHand[i - 1].AttackPower);
                Debug.Log("thisHand[i].AttackPower-1 = " + (thisHand[i].AttackPower-1));
                if (thisHand[i-1].AttackPower != thisHand[i].AttackPower-1)
                {
                    return false;
                }
            }

            //thisHand.HandType = TieredHandTypes(TieredHandTypesEnum.Straight);
            return true;
        }

    }

    private bool CheckIfStraightFlush(int attackPowerOf1stCard, string suitOf1stCard, List<CardScriptableObject> thisHand)
    {
        if (CheckIfStraight(attackPowerOf1stCard, thisHand) == true && CheckIfFlush(suitOf1stCard, thisHand) == true)
        {
            return true;
        }

        return false;
    }

    private bool CheckIfFourOfAKind(int attackPowerOf1stCard, List<CardScriptableObject> thisHand)
    {
        bool fourHigherNumber = false;
        bool fourLowerNumber = false;

        fourHigherNumber = thisHand[0].AttackPower == thisHand[1].AttackPower &&
            thisHand[1].AttackPower == thisHand[2].AttackPower &&
            thisHand[2].AttackPower == thisHand[3].AttackPower;

        fourLowerNumber = thisHand[1].AttackPower == thisHand[2].AttackPower &&
            thisHand[2].AttackPower == thisHand[3].AttackPower &&
            thisHand[3].AttackPower == thisHand[4].AttackPower;

        return fourHigherNumber || fourLowerNumber;
    }

    private bool CheckIfThreeOfAKind(int attackPowerOf1stCard, List<CardScriptableObject> thisHand)
    {
        bool threeHigherNumber = false;
        bool threeLowerNumber = false;

        threeHigherNumber = thisHand[0].AttackPower == thisHand[1].AttackPower &&
            thisHand[1].AttackPower == thisHand[2].AttackPower;

        threeLowerNumber = thisHand[1].AttackPower == thisHand[2].AttackPower &&
            thisHand[2].AttackPower == thisHand[3].AttackPower;

        return threeHigherNumber || threeLowerNumber;
    }

    private bool CheckIfOnePair(int attackPowerOf1stCard, List<CardScriptableObject> thisHand)
    {
        bool twoHigherNumber = false;
        bool twoLowerNumber = false;

        twoHigherNumber = thisHand[0].AttackPower == thisHand[1].AttackPower &&
            thisHand[1].AttackPower == thisHand[2].AttackPower;

        twoLowerNumber = thisHand[1].AttackPower == thisHand[2].AttackPower &&
            thisHand[2].AttackPower == thisHand[3].AttackPower;

        return twoHigherNumber || twoLowerNumber;
    }
    #endregion

    #endregion
}
