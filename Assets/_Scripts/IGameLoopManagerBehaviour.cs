﻿using System.Collections.Generic;

public interface IGameLoopManagerBehaviour
{
    List<CardScriptableObject> AllListOfCards { get; }

    void Shuffle();
    void DistributeCards();
    void CompareHands();
    int TieredHandTypes(string thisHand);
}
