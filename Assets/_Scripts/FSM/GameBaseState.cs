﻿public abstract class GameBaseState {
    public abstract void EnterState(GameLoop_FSM fsm);

    public abstract void UpdateState(GameLoop_FSM fsm);
}
