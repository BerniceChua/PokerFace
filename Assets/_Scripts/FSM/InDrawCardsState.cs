﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InDrawCardsState : GameBaseState
{
    public override void EnterState(GameLoop_FSM fsm)
    {
        fsm.m_GameManagerBehaviour.DistributeCards();
        fsm.m_CardsUIManager.ShowCardsPanels();
        //fsm.m_UIManager.SetCurrentSelectedGameObject();
    }

    public override void UpdateState(GameLoop_FSM fsm)
    {
        throw new System.NotImplementedException();
    }
}
