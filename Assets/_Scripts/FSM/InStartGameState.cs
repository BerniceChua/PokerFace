﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InStartGameState : GameBaseState
{
    public override void EnterState(GameLoop_FSM fsm)
    {
        //fsm.m_GameManagerBehaviour.DistributeCards
        fsm.m_UIManager.GetShuffleButton.gameObject.SetActive(true);

        fsm.m_UIManager.SetCurrentSelectedGameObject(fsm.m_UIManager.GetShuffleButton.gameObject);
    }

    public override void UpdateState(GameLoop_FSM fsm)
    {
        throw new System.NotImplementedException();
    }
}
