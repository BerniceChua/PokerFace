﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class InIntroState : GameBaseState
{
    public override void EnterState(GameLoop_FSM fsm)
    {
        fsm.m_UIManager.GetIntroPanel.SetActive(true);
        fsm.m_UIManager.GetGamePlayPanel.SetActive(false);

        Button okButton = fsm.m_UIManager.GetOKButton;
        fsm.m_UIManager.SetCurrentSelectedGameObject(okButton.gameObject);
    }

    public override void UpdateState(GameLoop_FSM fsm)
    {
        //if (fsm.m_replayButton.onClick.AddListener(fsm.ClickedReplayButton(true) )
        //{
        //    fsm.TransitionToState(fsm.ShuffleCardsState);
        //    fsm.ClickedReplayButton(false);
        //}

        fsm.TransitionToState(fsm.QuitState);
    }
}
