﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameLoop_FSM : MonoBehaviour
{
    #region Related To State Machine
    private GameBaseState currentState;
    public GameBaseState CurrentState {
        get { return currentState; }
    }

    public readonly InIntroState IntroState = new InIntroState();
    public readonly InShuffledCardsState ShuffleCardsState = new InShuffledCardsState();
    public readonly InDrawCardsState DrawCardsState = new InDrawCardsState();
    public readonly InQuitState QuitState = new InQuitState();
    #endregion

    public GameLoopManagerBehaviour m_GameManagerBehaviour;
    public UIManager m_UIManager;
    public CardsUIManager m_CardsUIManager;

    // Start is called before the first frame update
    void Start()
    {
        //m_replayButton.onClick.AddListener(ClickedReplayButton());

        //m_quitButton.onClick.AddListener();

        TransitionToState(IntroState);
    }

    // Update is called once per frame
    void Update()
    {
        currentState.UpdateState(this);
    }

    public void TransitionToState(GameBaseState state)
    {
        currentState = state;
        currentState.EnterState(this);
    }

    public void ClickedReplayButton(bool clickedReplayButton)
    {
        //m_bClickedReplayButton = clickedReplayButton;
    }
}
