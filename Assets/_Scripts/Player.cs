﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

[System.Serializable]
public class Player
{
    public PlayerScriptableObject m_PlayerScriptableObject;

    //public List<CardScriptableObject> m_CardsInHandList = new List<CardScriptableObject>();
    public List<CardScriptableObject> m_CardsInHandList;

    public void ResetPlayerHand(List<CardScriptableObject> cardsInHand)
    {
        m_PlayerScriptableObject.m_PlayerHand.Clear();
    }

    public void AddCard(CardScriptableObject card, List<CardScriptableObject> cardsInHand)
    {
        cardsInHand.Add(card);
    }

    public void AddCards(List<CardScriptableObject> allCardsList, List<CardScriptableObject> cardsInHand)
    {
        for (int i = 0; i < 5; i++)
        {
            cardsInHand.Add(allCardsList[i]);
        }
    }

    public void ShowCards()
    {
        foreach (var card in m_CardsInHandList)
        {
            Debug.WriteLine("card = " + card.GetCardName);
        }

        Debug.WriteLine(m_CardsInHandList.Count);
    }
}
