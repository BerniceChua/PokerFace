﻿using System.Collections;
using System.Collections.Generic;
using System;

public enum CardSuitsEnum
{
    Spades,
    Clubs,
    Hearts,
    Diamonds,
    Jokers,
}
