﻿using System;

[Serializable]
public class StringReference
{
    public bool UseContant = true;
    public string ConstantValue;
    public StringVariable Variable;

    public string Value
    {
        get { return UseContant ? ConstantValue : Variable.Value; }
    }
}
