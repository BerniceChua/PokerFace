﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerBehaviour : MonoBehaviour, IPlayerBehaviour
{
    public Player m_Player = new Player();

    Player IPlayerBehaviour.m_Player
    {
        get => m_Player;
        set => m_Player = value;
    }

    private void Start()
    {
        ResetPlayerHand(m_Player.m_PlayerScriptableObject.m_PlayerHand);

        m_Player.m_CardsInHandList = m_Player.m_PlayerScriptableObject.m_PlayerHand;
    }

    public void ResetPlayerHand(List<CardScriptableObject> cardsInHand)
    {
        m_Player.ResetPlayerHand(cardsInHand);
    }

    public void AddCardToHand(CardScriptableObject card, List<CardScriptableObject> cardsInHand)
    {
        m_Player.AddCard(card, cardsInHand);
    }

    public void AddCardsToHand(List<CardScriptableObject> allCardsList, List<CardScriptableObject> cardsInHand)
    {
        m_Player.AddCards(allCardsList, cardsInHand);
    }

    public void ShowAllCardsInHand()
    {
        m_Player.ShowCards();
    }
}
