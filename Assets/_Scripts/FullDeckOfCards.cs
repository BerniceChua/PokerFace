﻿using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class FullDeckOfCards
{
    public List<CardScriptableObject> m_AllCards;

    static List<CardScriptableObject> m_FullDeck;

    public void GetFullDeck()
    {
        m_FullDeck = m_AllCards;
    }
}
