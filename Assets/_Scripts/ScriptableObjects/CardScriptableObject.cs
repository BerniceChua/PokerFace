﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "CardScriptableObject", menuName = "ScriptableObject/CardScriptableObject")]
public class CardScriptableObject : ScriptableObject
{
    private bool m_bAllowJoker = false;
    /// <summary>
    /// Whether or not the Joker card is allowed is set by the GameManager.
    /// </summary>
    [HideInInspector] public bool bAllowJoker
    {
        set { m_bAllowJoker = value; }
    }

    private string m_addNotWord;

    private int m_maxNumber = 13;
    public int GetMaxNumber()
    {
        //get
        //{
            if (m_bAllowJoker == true)
            {
                m_maxNumber = 14;
                m_addNotWord = " ";
            }
            else
            {
                m_maxNumber = 13;
                m_addNotWord = " NOT ";
            }

            return m_maxNumber;
        //}
    }

    [SerializeField] private CardSuitsEnum m_suit;
    public void CardSuitSetterForTests(CardSuitsEnum cardSuit)
    {
        m_suit = cardSuit;
    }

    public string Suit => m_suit.ToString();

    [SerializeField]
    [Range(1, 13)] private int m_number;
    public void NumberSetterForTests(int number)
    {
        m_number = number;
    }

    public int GetNumber => m_number;

    private string m_cardName;
    public string GetCardName
    {
        get
        {
            string faceValue;

            //if (m_Number == 1)
            //{
            //    cardNum = "Ace";
            //}

            //if (m_Number == 11)
            //{
            //    cardNum = "Jack";
            //}

            //if (m_Number == 12)
            //{
            //    cardNum = "Queen";
            //}

            //if (m_Number == 13)
            //{
            //    cardNum = "King";
            //}

            if (m_number >= 1 && m_number <= m_maxNumber)
            {
                if (m_bAllowJoker == true && m_number == 14)
                {
                    faceValue = "Joker";
                }

                switch (m_number)
                {
                    case 1:
                        faceValue = "Ace";
                        break;
                    case 11:
                        faceValue = "Jack";
                        break;
                    case 12:
                        faceValue = "Queen";
                        break;
                    case 13:
                        faceValue = "King";
                        break;
                    default:
                        faceValue = m_number.ToString();
                        break;
                }

                m_cardName = faceValue + " of " + m_suit;
                Debug.Log(m_cardName);
            }
            else
            {
                Debug.LogError("Card Number must only be between 1 and " + m_maxNumber + ", if Joker card is" + m_addNotWord + "allowed.");
            }

            return m_cardName;
        }
    }

    public int AttackPower
    {
        get
        {
            int attackPower;

            switch (m_number)
            {
                case 1: // Ace
                    attackPower = 14;
                    break;
                case 13: // King
                    attackPower = 13;
                    break;
                case 12: // Queen
                    attackPower = 12;
                    break;
                case 11: // Jack
                    attackPower = 11;
                    break;
                default:
                    attackPower = m_number;
                    break;
            }

            return attackPower;
        }
    }

    //Texture2D myTexture = AssetPreview.GetAssetPreview(object);
    //GUILayout.Label(myTexture);
    [SerializeField] private Sprite m_cardFrontImage;
    public Sprite CardFrontImage
    {
        get { return m_cardFrontImage; }
    }

}
