﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "PlayerScriptableObject", menuName = "ScriptableObject/PlayerScriptableObject")]
public class PlayerScriptableObject : ScriptableObject
{
    public string m_PlayerName;

    public List<CardScriptableObject> m_PlayerHand = new List<CardScriptableObject>();

    public string m_HandType = "High Card / None";

    public int PowerTier = 1;

    [SerializeField] private int m_handTypePowerLevel = 0;
    public int GetHandTypePowerLevel { get => m_handTypePowerLevel; }

    public int CalculateHandTypePowerLevel(int cardAttackPower)
    {
        m_handTypePowerLevel = cardAttackPower;
        return m_handTypePowerLevel;
    }

    public void ShowCards()
    {
        foreach (var card in m_PlayerHand)
        {
            Debug.Log("card = " + card.GetCardName);
        }

        Debug.Log(m_PlayerHand.Count);
    }

    public void AddCard(CardScriptableObject card, List<CardScriptableObject> cardsInHand)
    {
        cardsInHand.Add(card);
    }
}
