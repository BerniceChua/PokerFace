﻿using UnityEngine;

[CreateAssetMenu(fileName = "FloatVariable", menuName = "ScriptableObject/FloatVariable")]
public class IntVariable : ScriptableObject
{
    public int Value;
}
