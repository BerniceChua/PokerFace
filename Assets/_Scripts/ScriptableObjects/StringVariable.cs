﻿using UnityEngine;

[CreateAssetMenu(fileName = "FloatVariable", menuName ="ScriptableObject/FloatVariable")]
public class StringVariable : ScriptableObject
{
    public string Value;
}
