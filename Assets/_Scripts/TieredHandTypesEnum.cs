﻿public enum TieredHandTypesEnum
{
    Five_Of_A_Kind,
    Straight_Flush,
    Four_Of_A_Kind,
    Full_House,
    Flush,
    Straight,
    Three_Of_A_Kind,
    Two_Pair,
    One_Pair,
    High_Card_aka_No_Pair,
}
