﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class CardBehaviour : MonoBehaviour
{
    [SerializeField] public CardScriptableObject m_cardSO;

    [SerializeField] private IntVariable m_number;
    [SerializeField] private StringVariable m_suit;
    [SerializeField] private StringVariable m_cardName;
    [SerializeField] private IntVariable m_attackPower;
    [SerializeField] private Sprite m_cardFrontImage;

    //[SerializeField] private TMP_Text m_number;
    //[SerializeField] private TMP_Text m_suit;
    //[SerializeField] private TMP_Text m_cardName;
    //[SerializeField] private Image m_cardFrontImage;


    private MeshRenderer m_mesh;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("m_cardSO.m_Number = " + m_cardSO.GetNumber);
        Debug.Log("m_cardSO.Suit = " + m_cardSO.Suit);
        Debug.Log("m_cardSO.GetCardName = " + m_cardSO.GetCardName);
        Debug.Log("m_cardSO.AttackPower = " + m_cardSO.AttackPower);
        Debug.Log("m_cardSO.CardFrontImage = " + m_cardSO.CardFrontImage);

        //m_number.text = m_cardSO.m_Number.ToString();
        //m_suit.text = m_cardSO.Suit;
        //m_cardName.text = m_cardSO.GetCardName;
        //m_cardFrontImage.sprite = m_cardSO.CardFrontImage;

        m_mesh = GetComponentInChildren<MeshRenderer>();

        //SpriteToMeshRenderer(m_cardSO.CardFrontImage);
    }

    public void Card(IntVariable number, StringVariable suit, StringVariable cardName, IntVariable attackPower, Sprite cardFrontImage)
    {
        m_number = number;
        m_suit = suit;
        m_cardName = cardName;
        m_attackPower = attackPower;
        m_cardFrontImage = cardFrontImage;
    }

    //Mesh SpriteToMeshRenderer(Sprite sprite)
    //{
    //    Material[] mat = new Material[1];
    //    m_mesh.vertices = Array.ConvertAll(sprite.vertices, i => (Vector3)i);
    //    m_mesh.uv = sprite.uv;
    //    m_mesh.triangles = Array.ConvertAll(sprite.triangles, i => (int)i);

    //    return m_mesh;
    //}
}
