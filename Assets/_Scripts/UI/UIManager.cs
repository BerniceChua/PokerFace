﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using UnityEngine.EventSystems;

public class UIManager : MonoBehaviour
{
    [Header("UI List")]
    #region UI List
    #region Intro
    [SerializeField] GameObject m_introPanel;
    public GameObject GetIntroPanel => m_introPanel;

    [SerializeField] GameObject m_gameplayPanel;
    public GameObject GetGamePlayPanel => m_gameplayPanel;

    [SerializeField] Button m_replayButton;
    public Button ReplayButton => m_replayButton;
    private bool m_bClickedReplayButton;

    [SerializeField] private Button m_okButton;
    public Button GetOKButton => m_okButton;

    [SerializeField] private Button m_quitButton;
    public Button GetQuitButton => m_quitButton;
    #endregion

    #region StartGame
    [SerializeField] private Button m_shuffleButton;
    public Button GetShuffleButton => m_shuffleButton;
    #endregion

    #region Shuffle
    [SerializeField] private GameObject m_drawCardsGObject;
    public GameObject GetDrawCardsGObject => m_drawCardsGObject;

    //[SerializeField] private GameObject m_drawCardsGObject;
    //public GameObject GetDrawCardsGObject => m_drawCardsGObject;

    [SerializeField] private Button m_drawCardsButton;
    public Button GetDrawCardsButton => m_drawCardsButton;
    #endregion

    #region Quit
    [SerializeField] private GameObject m_thxForPlayingPanel;
    public GameObject ThxForPlayingPanel => m_thxForPlayingPanel;

    [SerializeField]

    #endregion

    #endregion


    [Header("Component References")]
    public EventSystem eventSystem;
    public InputSystemUIInputModule inputSystemUIInputModule;

    public void SetCurrentSelectedGameObject(GameObject newSelectedGameObject)
    {
        eventSystem.SetSelectedGameObject(newSelectedGameObject);
        Button newSelectable = newSelectedGameObject.GetComponent<Button>();
        newSelectable.Select();
        newSelectable.OnSelect(null);
    }

    public InputActionAsset GetInputActionAsset()
    {
        return inputSystemUIInputModule.actionsAsset;
    }

    public void PutURL()
    {
        Application.OpenURL("https://gitlab.com/BerniceChua/PokerFace");
    }
}
