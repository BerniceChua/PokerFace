﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SliderSetter : MonoBehaviour
{
    [SerializeField] private GameLoopManagerBehaviour m_gameManager;

    public StringVariable StringVar;
    public IntVariable IntVar;

    public Slider thisSlider;

    // Start is called before the first frame update
    void Start()
    {
        if (FindObjectOfType<GameLoopManagerBehaviour>() != null)
        {
            m_gameManager = FindObjectOfType<GameLoopManagerBehaviour>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        //thisSlider.value = Mathf.Clamp01(IntVar.Value / m_gameManager.m_MaxPlayers.Value);
    }

    public void SetSlider()
    {
        thisSlider.value = IntVar.Value;
    }
}
