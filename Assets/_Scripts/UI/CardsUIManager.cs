﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CardsUIManager : MonoBehaviour
{
    [SerializeField] private GameObject m_player1Panel;
    [SerializeField] private GameObject m_player2Panel;

    [SerializeField] private List<Image> m_player1Images;
    [SerializeField] private List<Image> m_player2Images;

    [SerializeField] private GameLoopManagerBehaviour m_gameManager;

    //[SerializeField] private List<CardScriptableObject> m_cards = new List<CardScriptableObject>();
    [SerializeField] private PlayerScriptableObject m_player1SO;
    [SerializeField] private PlayerScriptableObject m_player2SO;

    [SerializeField] private GameObject m_announceWinnerPanel;
    [SerializeField] private TMP_Text m_announceWinnerText;

    // Start is called before the first frame update
    void Start()
    {
        //foreach (var card in m_gameManager.Player1.m_CardsInHandList)
        //foreach (var card in m_gameManager.Player1.m_Player.m_CardsInHandList)
        //{
        //    m_cards.Add(card);
        //}

    }

    //// Update is called once per frame
    //void Update()
    //{
        
    //}

    public void ShowCardsPanels()
    {
        m_player1Panel.SetActive(true);
        m_player2Panel.SetActive(true);

        PutCardValuesIntoUI();
    }

    public void PutCardValuesIntoUI()
    {
        if (m_player1SO != null)
        {
            for (int i = 0; i < m_player1Images.Count; i++)
            {
                m_player1Images[i].sprite = m_player1SO.m_PlayerHand[i].CardFrontImage;
            }
        }

        if (m_player2SO != null)
        {
            for (int i = 0; i < m_player2Images.Count; i++)
            {
                m_player2Images[i].sprite = m_player2SO.m_PlayerHand[i].CardFrontImage;
            }
        }
    }

    public void SortCardValuesIntoUI()
    {
        if (m_player1SO != null)
        {
            for (int i = 0; i < m_player1Images.Count; i++)
            {
                m_player1Images[i].sprite = m_player1SO.m_PlayerHand[i].CardFrontImage;
            }
        }

        if (m_player2SO != null)
        {
            for (int i = 0; i < m_player2Images.Count; i++)
            {
                m_player2Images[i].sprite = m_player2SO.m_PlayerHand[i].CardFrontImage;
            }
        }
    }

    public void HideCardsPanels()
    {
        m_player1Panel.SetActive(false);
        m_player2Panel.SetActive(false);
    }

    public void AnnounceWinner()
    {
        PlayerScriptableObject winner = m_gameManager.Winner();

        if (winner == null)
        {
            Debug.Log("It's a tie!!!");
            m_announceWinnerText.text = "It's a tie!!!";
        }
        else
        {
            Debug.Log(winner.m_PlayerName.ToString() + " won with a " + winner.m_HandType.ToString() + ".");
            m_announceWinnerText.text = winner.m_PlayerName.ToString() + " won \n with a " + winner.m_HandType.ToString() + ".";
        }
    }
}
