﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FullDeckOfCardsBehaviour : MonoBehaviour
{
    public FullDeckOfCards m_FullDeckOfCards = new FullDeckOfCards();

    // Start is called before the first frame update
    void Start()
    {
        List<CardScriptableObject> m_fullDeck = new List<CardScriptableObject>();

        for (int i = 0; i < 13; i++)
        {
            m_fullDeck.Add(CardScriptableObject.CreateInstance<CardScriptableObject>() );
            m_fullDeck[i].NumberSetterForTests(i + 1);
            m_fullDeck[i].CardSuitSetterForTests(CardSuitsEnum.Clubs);
            Debug.Log(m_fullDeck[i].GetCardName);
        }

        for (int i = 13; i < 26; i++)
        {
            m_fullDeck.Add(CardScriptableObject.CreateInstance<CardScriptableObject>() );
            m_fullDeck[i].NumberSetterForTests((i - 13) + 1);
            m_fullDeck[i].CardSuitSetterForTests(CardSuitsEnum.Diamonds);
            Debug.Log(m_fullDeck[i].GetCardName);
        }

        for (int i = 26; i < 39; i++)
        {
            m_fullDeck.Add(CardScriptableObject.CreateInstance<CardScriptableObject>() );
            m_fullDeck[i].NumberSetterForTests((i - 26) + 1);
            m_fullDeck[i].CardSuitSetterForTests(CardSuitsEnum.Hearts);
            Debug.Log(m_fullDeck[i].GetCardName);
        }

        for (int i = 39; i < 52; i++)
        {
            m_fullDeck.Add(CardScriptableObject.CreateInstance<CardScriptableObject>() );
            m_fullDeck[i].NumberSetterForTests((i - 39) + 1);
            m_fullDeck[i].CardSuitSetterForTests(CardSuitsEnum.Spades);
            Debug.Log(m_fullDeck[i].GetCardName);
        }

        Debug.Log("m_fullDeck.Count = " + m_fullDeck.Count);
    }

    //// Update is called once per frame
    //void Update()
    //{

    //}
}
