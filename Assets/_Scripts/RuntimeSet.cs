﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RuntimeSet<T> : ScriptableObject
{
    public List<T> Items = new List<T>();

    //// Start is called before the first frame update
    //void Start()
    //{
        
    //}

    //// Update is called once per frame
    //void Update()
    //{
        
    //}

    public void Add(T thisType)
    {
        if (Items.Contains(thisType) == false)
        {
            Items.Add(thisType);
        }
    }

    public void Remove(T thisType)
    {
        if (Items.Contains(thisType) == true)
        {
            Items.Remove(thisType);
        }
    }
}
