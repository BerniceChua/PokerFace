﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
//using System.Diagnostics;
using UnityEngine;

[Serializable]
public class GameLoopManager
{
    public IntReference m_HandSize;

    public IntReference m_MaxPlayers;
    public IntReference m_MinPlayers;

    /// <summary>
    /// For use in "Shuffle()" method
    /// </summary>
    public System.Random m_Randomizer = new System.Random();

    public void Shuffle(List<CardScriptableObject> allListOfCards)
    {
        int n = allListOfCards.Count;
        while (n > 1)
        {
            n--;
            int k = m_Randomizer.Next(n + 1);
            CardScriptableObject value = allListOfCards[k];
            allListOfCards[k] = allListOfCards[n];
            allListOfCards[n] = value;
        }
    }

    //public void DistributeCards(List<CardScriptableObject> allListOfCards, PlayerBehaviour thisPlayer)
    //public void DistributeCards(List<CardScriptableObject> allListOfCards, IPlayerBehaviour thisPlayer, int position)
    //public void DistributeCards(List<CardScriptableObject> allListOfCards, Player thisPlayer, int position)
    //public void DistributeCards(List<CardScriptableObject> allListOfCards, IPlayerBehaviour thisPlayer, int position)
    //{
    //    //thisPlayer.AddCardsToHand(allListOfCards, thisPlayer.m_Player.m_PlayerScriptableObject.m_PlayerHand);
    //    for (int i = 0+position; i < 5+position; i++)
    //    {
    //        thisPlayer.AddCardToHand(allListOfCards[i], thisPlayer.m_Player.m_PlayerScriptableObject.m_PlayerHand);
    //    }
    //}
    public void DistributeCards(List<CardScriptableObject> allListOfCards, PlayerScriptableObject thisPlayer, int position)
    {
        //thisPlayer.AddCardsToHand(allListOfCards, thisPlayer.m_Player.m_PlayerScriptableObject.m_PlayerHand);
        for (int i = 0 + position; i < 5 + position; i++)
        {
            thisPlayer.AddCard(allListOfCards[i], thisPlayer.m_PlayerHand);
        }
    }


    /// <summary>
    /// Compare 2 Hands:
    /// Step 1: sort each hand by descending order
    /// Step 2: identify what's in each hand
    /// Step 3: compare each hand to each other
    /// Step 4: identify the winner
    /// Step 5: return the player who won the round
    /// </summary>
    /// <param name="player1Hand"></param>
    /// <param name="player2Hand"></param>
    //public void Compare2Hands(PlayerScriptableObject player1SO, PlayerScriptableObject player2SO)
    public PlayerScriptableObject Compare2Hands(PlayerScriptableObject player1SO, PlayerScriptableObject player2SO)
    {
        /// Step 1: sort each hand by descending order.
        //List<CardScriptableObject> sortedHand = thisHand.OrderByDescending(o => o.AttackPower).ToList();
        //foreach (var item in sortedHand)
        //{
        //    Debug.Log(item);
        //}
        List<CardScriptableObject> p1SortedHand = SortByAttackPower(player1SO.m_PlayerHand);
        player1SO.m_PlayerHand = p1SortedHand;

        List<CardScriptableObject> p2SortedHand = SortByAttackPower(player2SO.m_PlayerHand);
        player2SO.m_PlayerHand = p2SortedHand;

        /// Step 2: identify what's in each hand
        int p1PowerLVL = WhatIsInThisHand(player1SO, p1SortedHand);
        player1SO.PowerTier = p1PowerLVL;

        int p2PowerLVL = WhatIsInThisHand(player2SO, p2SortedHand);
        player2SO.PowerTier = p2PowerLVL;

        /// Step 3: compare each hand to each other
        /// Step 4: identify the winner
        /// Step 5: return the player who won the round
        return FindWinner(player1SO, player2SO);
    }

    public void PlayAgainLogic(PlayerBehaviour[] m_listOfPlayers)
    {
        foreach (IPlayerBehaviour player in m_listOfPlayers)
        {
            player.ResetPlayerHand(player.m_Player.m_CardsInHandList);
        }
    }

    #region Comparison-Logic
    /// <summary>
    /// Without the Joker cards, 
    /// 'Straight Flush' has the highest power level,
    /// followed by 'Flush', etc,
    /// with 'High Card aka None' being the lowest power level.
    /// </summary>
    /// <param name="thisPlayer"></param>
    /// <param name="sortedHand"></param>
    /// <returns>integer that represents the power level of a poker hand</returns>
    public int WhatIsInThisHand(PlayerScriptableObject thisPlayer, List<CardScriptableObject> sortedHand)
    {
        /// Step 2: compare each of the cards to each other, see how many cards match the 1st card.
        /// If cards continue to match, it's a 4 of a kind or 3 of a kind or 2 of a kind, etc.  If there's a tie, the Attack Power of each card will be the tie breaker
        /// For 3 of a kind check if the next 2 cards match because that will make a full house.
        CardScriptableObject strongestCard = sortedHand[0];
        string strongestCardName = strongestCard.GetCardName;
        int strongestCardATK = strongestCard.AttackPower;
        string strongestCardSuit = strongestCard.Suit;
        //Debug.WriteLine("strongest card name = " + strongestCardName);
        Debug.Log("strongest card name = " + strongestCardName);

        //Debug.WriteLine(CheckIfFlush(strongestCardSuit, sortedHand));
        //Debug.Log(CheckIfFlush(strongestCardSuit, sortedHand));
        //Debug.WriteLine(CheckIfStraight(strongestCardATK, sortedHand));
        //Debug.Log(CheckIfStraight(strongestCardATK, sortedHand));
        //CheckIfFlush(strongestCardSuit, sortedHand);
        //CheckIfStraight(strongestCardATK, sortedHand);

        string handType = string.Empty;

        //if (CheckIfStraightFlush(strongestCardATK, strongestCardSuit, sortedHand))
        if (CheckIfStraightFlush(thisPlayer) == true)
        {
            handType = string.Join(" ", TieredHandTypesEnum.Straight_Flush.ToString().Split('_'));
            //Debug.WriteLine(string.Join(" ", TieredHandTypesEnum.Straight_Flush.ToString().Split('_')));
            Debug.Log(string.Join(" ", TieredHandTypesEnum.Straight_Flush.ToString().Split('_'))); thisPlayer.m_HandType = handType;
            thisPlayer.PowerTier = TieredHandTypes(handType);
            //Debug.WriteLine("handType = " + handType);
            Debug.Log(thisPlayer.m_PlayerName + "'s handType = " + handType);
        }
        //else if (CheckIfFlush(strongestCardSuit, sortedHand) == true)
        else if (CheckIfFlush(thisPlayer) == true)
        {
            handType = string.Join(" ", TieredHandTypesEnum.Flush.ToString().Split('_'));
            //Debug.WriteLine(string.Join(" ", TieredHandTypesEnum.Flush.ToString().Split('_')));
            Debug.Log(string.Join(" ", TieredHandTypesEnum.Flush.ToString().Split('_')));
            thisPlayer.m_HandType = handType;
            thisPlayer.PowerTier = TieredHandTypes(handType);
            //Debug.WriteLine("handType = " + handType);
            Debug.Log(thisPlayer.m_PlayerName + "'s handType = " + handType);
        }
        //else if (CheckIfStraight(strongestCardATK, sortedHand) == true)
        else if (CheckIfStraight(thisPlayer) == true)
        {
            handType = string.Join(" ", TieredHandTypesEnum.Straight.ToString().Split('_'));
            //Debug.WriteLine(string.Join(" ", TieredHandTypesEnum.Straight.ToString().Split('_')));
            Debug.Log(string.Join(" ", TieredHandTypesEnum.Straight.ToString().Split('_'))); thisPlayer.m_HandType = handType;
            thisPlayer.PowerTier = TieredHandTypes(handType);
            //Debug.WriteLine("handType = " + handType);
            Debug.Log(thisPlayer.m_PlayerName + "'s handType = " + handType);
        }
        //else if (CheckIfFourOfAKind(strongestCardATK, sortedHand) == true)
        else if (CheckIfFourOfAKind(thisPlayer) == true)
        {
            handType = string.Join(" ", TieredHandTypesEnum.Four_Of_A_Kind.ToString().Split('_'));
            //Debug.WriteLine(string.Join(" ", TieredHandTypesEnum.Four_Of_A_Kind.ToString().Split('_')));
            Debug.Log(string.Join(" ", TieredHandTypesEnum.Four_Of_A_Kind.ToString().Split('_'))); thisPlayer.m_HandType = handType;
            thisPlayer.PowerTier = TieredHandTypes(handType);
            //Debug.WriteLine("handType = " + handType);
            Debug.Log(thisPlayer.m_PlayerName + "'s handType = " + handType);
        }
        //else if (CheckIfFullHouse(strongestCardATK, sortedHand) == true)
        else if (CheckIfFullHouse(thisPlayer) == true)
        {
            handType = string.Join(" ", TieredHandTypesEnum.Three_Of_A_Kind.ToString().Split('_'));
            //Debug.WriteLine(string.Join(" ", TieredHandTypesEnum.Three_Of_A_Kind.ToString().Split('_')));
            Debug.Log(string.Join(" ", TieredHandTypesEnum.Three_Of_A_Kind.ToString().Split('_'))); thisPlayer.m_HandType = handType;
            thisPlayer.PowerTier = TieredHandTypes(handType);
            //Debug.WriteLine("handType = " + handType);
            Debug.Log(thisPlayer.m_PlayerName + "'s handType = " + handType);
        }
        //else if (CheckIfThreeOfAKind(strongestCardATK, sortedHand) == true)
        else if (CheckIfThreeOfAKind(thisPlayer) == true)
        {
            handType = string.Join(" ", TieredHandTypesEnum.Three_Of_A_Kind.ToString().Split('_'));
            //Debug.WriteLine(string.Join(" ", TieredHandTypesEnum.Three_Of_A_Kind.ToString().Split('_')));
            Debug.Log(string.Join(" ", TieredHandTypesEnum.Three_Of_A_Kind.ToString().Split('_'))); thisPlayer.m_HandType = handType;
            thisPlayer.PowerTier = TieredHandTypes(handType);
            //Debug.WriteLine("handType = " + handType);
            Debug.Log(thisPlayer.m_PlayerName + "'s handType = " + handType);
        }
        //else if (CheckIf2Pairs(strongestCardATK, sortedHand) == true)
        else if (CheckIf2Pairs(thisPlayer) == true)
        {
            handType = string.Join(" ", TieredHandTypesEnum.Two_Pair.ToString().Split('_'));
            thisPlayer.m_HandType = handType;
            thisPlayer.PowerTier = TieredHandTypes(handType);
            Debug.Log(thisPlayer.m_PlayerName + "'s handType = " + handType);
        }
        //else if (CheckIfOnePair(sortedHand) == true)
        else if (CheckIfOnePair(thisPlayer/*, sortedHand*/) == true)
        {
            //Debug.Log("~~~~~~~~~~~~~~~~~~~~~~~yay!!!~~~~~~~~~~~~~~~~~~~~~~~~");
            handType = string.Join(" ", TieredHandTypesEnum.One_Pair.ToString().Split('_'));
            thisPlayer.m_HandType = handType;
            thisPlayer.PowerTier = TieredHandTypes(handType);
            Debug.Log(thisPlayer.m_PlayerName + "'s handType = " + handType);
        }
        else
        {
            handType = string.Join(" ", TieredHandTypesEnum.High_Card_aka_No_Pair.ToString().Split('_'));
            //Debug.WriteLine(string.Join(" ", TieredHandTypesEnum.High_Card_aka_No_Pair_aka_None.ToString().Split('_')));
            Debug.Log(string.Join(" ", TieredHandTypesEnum.High_Card_aka_No_Pair.ToString().Split('_')));
            thisPlayer.m_HandType = handType;
            thisPlayer.PowerTier = TieredHandTypes(handType);
            //Debug.WriteLine("handType = " + handType);
            Debug.Log(thisPlayer.m_PlayerName + "'s handType = " + handType);
        }

        return thisPlayer.PowerTier;
    }

    //public PlayerScriptableObject FindWinner(List<CardScriptableObject> player1Hand, List<CardScriptableObject> player2Hand)
    //public void FindWinner(List<CardScriptableObject> player1Hand, List<CardScriptableObject> player2Hand)
    public PlayerScriptableObject FindWinner(PlayerScriptableObject p1, PlayerScriptableObject p2)
    {
        PlayerScriptableObject winner = null;

        if (p1.PowerTier > p2.PowerTier)
        {
            winner = p1;
            //Debug.WriteLine("The winner is = " + p1.name);
            Debug.Log("The winner is = " + p1.name);
        }
        else if (p1.PowerTier < p2.PowerTier)
        {
            winner = p2;
            //Debug.WriteLine("The winner is = " + p2.name);
            Debug.Log("The winner is = " + p2.name);
        }
        else if (p1.PowerTier == p2.PowerTier)
        {
            if (p1.PowerTier == 1 && p2.PowerTier == 1)
            {
                winner = CompareHighCards(p1, p2);
            }
            else
            {
                winner = TieBreaker(p1, p2);
            }
        }
        else
        {
            Debug.Log("No Winner");
            winner = null;
        }

        return winner;
    }

    //public PlayerScriptableObject Winner()
    //{
    //    return FindWinner(m_player1so, player2SO);
    //}

    /// <summary>
    /// Each high card hand is ranked first by the rank of its highest-ranking card, then by the rank of its second highest-ranking card, then by the rank of its third highest-ranking card, then by the rank of its fourth highest-ranking card, and finally by the rank of its lowest-ranking card. For example, K♠ 6♣ 5♥ 3♦ 2♣ ranks higher than Q♠ J♦ 6♣ 5♥ 3♣, which ranks higher than Q♠ 10♦ 8♣ 7♦ 4♠, which ranks higher than Q♥ 10♥ 7♣ 6♥ 4♠, which ranks higher than Q♣ 10♣ 7♦ 5♣ 4♦, which ranks higher than Q♥ 10♦ 7♠ 5♠ 2♥. High card hands that differ by suit alone, such as 10♣ 8♠ 7♠ 6♥ 4♦ and 10♦ 8♦ 7♠ 6♣ 4♣, are of equal rank.
    /// </summary>
    /// <param name="player1"></param>
    /// <param name="player2"></param>
    /// <param name="position"></param>
    /// <returns>The PlayerScriptableObject player who wins</returns>
    public PlayerScriptableObject CompareHighCards(PlayerScriptableObject player1, PlayerScriptableObject player2, int position=0)
    {
        int handCount = player1.m_PlayerHand.Count;

        Debug.Log("player1 = " + player1);
        Debug.Log("player1.m_PlayerName = " + player1.m_PlayerName);
        Debug.Log("player1.m_PlayerHand.Count = " + player1.m_PlayerHand.Count);

        Debug.Log("player2 = " + player2);
        Debug.Log("player2.m_PlayerName = " + player2.m_PlayerName);
        Debug.Log("player2.m_PlayerHand.Count = " + player2.m_PlayerHand.Count);

        Debug.Log("position = " + position);
        //for (int i = 0; i < player1.m_PlayerHand.Count; i++)
        //{
        if ( (player1.m_PlayerHand[position].AttackPower > player2.m_PlayerHand[position].AttackPower) && (position < handCount) )
        {
            //Debug.WriteLine("The winner is = " + winner.name.ToString());
            //Debug.Log("The winner is = " + player1.m_PlayerName.ToString() + "!!  " + player1.m_PlayerName.ToString() + " won with a " + player1.m_HandType.ToString() + ".");

            return player1;
        }
        else if ( (player1.m_PlayerHand[position].AttackPower < player2.m_PlayerHand[position].AttackPower) && (position < handCount) )
        {
            //Debug.WriteLine("The winner is = " + winner.name.ToString());
            //Debug.Log("The winner is = " + player2.m_PlayerName.ToString() + "!!  " + player2.m_PlayerName.ToString() + " won with a " + player2.m_HandType.ToString() + ".");

            return player2;
        }
        else if( (player1.m_PlayerHand[position].AttackPower == player2.m_PlayerHand[position].AttackPower) && (position < handCount) )
        {
            return CompareHighCards(player1, player2, position+1);
        }
        else
        {
            return null;
        }
        
        //}
    }

    /// <summary>
    /// If the Hand Rank / Hand Type is the same,
    /// use the card's Attack Power as a tie breaker.
    /// </summary>
    //private void TieBreaker(PlayerScriptableObject p1, PlayerScriptableObject p2)
    public PlayerScriptableObject TieBreaker(PlayerScriptableObject p1, PlayerScriptableObject p2)
    {
        //int p1TotalAttackPower = 0;
        //int p2TotalAttackPower = 0;

        //for (int i = 0; i < p1.m_PlayerHand.Count; i++)
        //{
        //    //Debug.WriteLine(p1.m_PlayerHand[i].AttackPower);
        //    Debug.Log(p1.m_PlayerHand[i].AttackPower);
        //    p1TotalAttackPower += p1.m_PlayerHand[i].AttackPower;
        //    p2TotalAttackPower += p2.m_PlayerHand[i].AttackPower;
        //}

        ////Debug.WriteLine("p1TotalAttackPower = " + p1TotalAttackPower);
        ////Debug.WriteLine("p2TotalAttackPower = " + p1TotalAttackPower);
        //Debug.Log("p1TotalAttackPower = " + p1TotalAttackPower);
        //Debug.Log("p2TotalAttackPower = " + p1TotalAttackPower);

        //if (p1TotalAttackPower > p2TotalAttackPower)
        //{
        //    //Debug.WriteLine("The winner is = " + p1.name);
        //    Debug.Log("The winner is = " + p1.name);
        //    return p1;
        //}
        //else
        //{
        //    //Debug.WriteLine("The winner is = " + p2.name);
        //    Debug.Log("The winner is = " + p2.name);
        //    return p2;
        //}

        int p1HandTypePowerLevel = p1.GetHandTypePowerLevel;
        int p2HandTypePowerLevel = p2.GetHandTypePowerLevel;

        if (p1HandTypePowerLevel > p2HandTypePowerLevel)
        {
            //Debug.WriteLine("The winner is = " + p1.name);
            Debug.Log("The winner is = " + p1.m_PlayerName);

            //Debug.WriteLine("The winner is = " + winner.name.ToString());
            Debug.Log("The winner is = " + p1.m_PlayerName.ToString() + "!!  " + p1.m_PlayerName.ToString() + " won with a " + p1.m_HandType.ToString() + ".");

            return p1;
        }
        else if (p1HandTypePowerLevel < p2HandTypePowerLevel)
        {
            //Debug.WriteLine("The winner is = " + p2.name);
            Debug.Log("The winner is = " + p2.m_PlayerName);

            //Debug.WriteLine("The winner is = " + winner.name.ToString());
            Debug.Log("The winner is = " + p2.m_PlayerName.ToString() + "!!  " + p2.m_PlayerName.ToString() + " won with a " + p2.m_HandType.ToString() + ".");

            return p2;
        }
        else
        {
            return null;
        }
    }
    #endregion

    #region Helper Functions

    #region Sorters
    //private List<CardScriptableObject> SortByAttackPower(List<CardScriptableObject> thisHand)
    public List<CardScriptableObject> SortByAttackPower(List<CardScriptableObject> thisHand)
    {
        List<CardScriptableObject> sortByAttackPower = new List<CardScriptableObject>();

        List<CardScriptableObject> sortedHand = thisHand.OrderByDescending(o => o.AttackPower).ToList();
        foreach (var item in sortedHand)
        {
            //Debug.WriteLine(item.GetCardName);
            //Debug.Log(item.GetCardName);
            sortByAttackPower.Add(item);
        }

        return sortByAttackPower;
    }

    //private List<CardScriptableObject> SortByCardNumber(List<CardScriptableObject> thisHand)
    public List<CardScriptableObject> SortByCardNumber(List<CardScriptableObject> thisHand)
    {
        List<CardScriptableObject> sortByCardNumber = new List<CardScriptableObject>();

        List<CardScriptableObject> sortedHand = thisHand.OrderByDescending(o => o.GetNumber).ToList();
        foreach (var item in sortedHand)
        {
            //Debug.WriteLine(item.GetCardName);
            //Debug.Log(item.GetCardName);
            sortByCardNumber.Add(item);
        }

        return sortByCardNumber;
    }
    #endregion

    public int TieredHandTypes(string thisHand)
    {
        Dictionary<string, int> ranking = new Dictionary<string, int>
        {
            ["Five Of A Kind"] = 10,
            ["Straight Flush"] = 9,
            ["Four Of A Kind"] = 8,
            ["Full House"] = 7,
            ["Flush"] = 6,
            ["Straight"] = 5,
            ["Three Of A Kind"] = 4,
            ["Two Pair"] = 3,
            ["One Pair"] = 2,
            ["High Card aka No Pair"] = 1,
        };

        int value = 0;
        if (ranking.TryGetValue(thisHand, out value))
        {
            return value;
        }
        else
        {
            return 0;
        }
    }

    #region Check What Kind Of Hand

    /// <summary>
    /// Flush means all cards are the same suit.
    /// </summary>
    /// <param name="thisPlayer"></param>
    /// <returns>true if all cards have the same suit; false if not</returns>
    public bool CheckIfFlush(PlayerScriptableObject thisPlayer/*, List<CardScriptableObject> thisHand*/)
    {
        string suitOf1stCard = thisPlayer.m_PlayerHand[0].Suit;
        foreach (CardScriptableObject card in thisPlayer.m_PlayerHand)
        {
            if (card.Suit != suitOf1stCard)
            {
                return false;
            }
        }

        //Player1.PlayerScriptable.HandType = "Flush";
        //Debug.Log("Player1.HandType = " + Player1.PlayerScriptable.HandType);
        thisPlayer.CalculateHandTypePowerLevel(thisPlayer.m_PlayerHand[0].AttackPower);
        return true;
    }

    /// <summary>
    /// Straight means that all cards' Attack Power are sequential in descending order.
    /// </summary>
    /// <param name="thisPlayer"></param>
    /// <returns>true if all sequential descending order, false if not</returns>
    //public bool CheckIfStraight(int attackPowerOf1stCard, List<CardScriptableObject> thisHand)
    public bool CheckIfStraight(PlayerScriptableObject thisPlayer/*, List<CardScriptableObject> thisHand*/)
    {
        /// An ace-high straight, such as A♣ K♣ Q♦ J♠ 10♠, is called a Broadway straight
        bool bIsBroadwayStraight = false;
        /// A five-high straight, such as 5♠ 4♦ 3♦ 2♠ A♥, is called a baby straight
        bool bIsBabyStraight = false;

        /// Check if it contains an Ace
        //if (attackPowerOf1stCard == 14)
        if (thisPlayer.m_PlayerHand[0].AttackPower == 14)
        {
            //Debug.WriteLine("Check if contains Ace of 'Check If Straight'.");
            Debug.Log("Check if contains Ace of 'Check If Straight'.");

            bIsBroadwayStraight = (thisPlayer.m_PlayerHand[1].AttackPower == 13) &&
                (thisPlayer.m_PlayerHand[2].AttackPower == 12) &&
                (thisPlayer.m_PlayerHand[3].AttackPower == 11) &&
                (thisPlayer.m_PlayerHand[4].AttackPower == 10);
            if (bIsBroadwayStraight == true)
            {
                thisPlayer.CalculateHandTypePowerLevel(thisPlayer.m_PlayerHand[0].AttackPower);
            }

            bIsBabyStraight = (thisPlayer.m_PlayerHand[1].AttackPower == 5) &&
                (thisPlayer.m_PlayerHand[2].AttackPower == 4) &&
                (thisPlayer.m_PlayerHand[3].AttackPower == 3) &&
                (thisPlayer.m_PlayerHand[4].AttackPower == 2);
            if (bIsBabyStraight == true)
            {
                thisPlayer.CalculateHandTypePowerLevel(thisPlayer.m_PlayerHand[0].AttackPower);
            }

            //thisHand.HandType = TieredHandTypes(TieredHandTypesEnum.Straight);
            //Debug.WriteLine("bIsBroadwayStraight = " + bIsBroadwayStraight);
            Debug.Log("bIsBroadwayStraight = " + bIsBroadwayStraight);
            //Debug.WriteLine("bIsBabyStraight = " + bIsBabyStraight);
            Debug.Log("bIsBabyStraight = " + bIsBabyStraight);
            return bIsBroadwayStraight || bIsBabyStraight;
        }
        else
        {
            //Debug.WriteLine("inside the else of Check If Straight.");
            Debug.Log("inside the else of Check If Straight.");

            for (int i = 1; i < thisPlayer.m_PlayerHand.Count; i++)
            {
                //Debug.WriteLine("i = " + i);
                //Debug.WriteLine("thisHand[i-1].AttackPower = " + thisHand[i - 1].AttackPower);
                //Debug.WriteLine("thisHand[i].AttackPower-1 = " + (thisHand[i].AttackPower - 1));
                Debug.Log("i = " + i);
                Debug.Log("thisHand[i-1].AttackPower = " + thisPlayer.m_PlayerHand[i - 1].AttackPower);
                Debug.Log("thisHand[i].AttackPower + 1 = " + (thisPlayer.m_PlayerHand[i].AttackPower + 1));

                if ( (thisPlayer.m_PlayerHand[i - 1].AttackPower) != (thisPlayer.m_PlayerHand[i].AttackPower + 1) )
                {
                    return false;
                }
            }

            thisPlayer.CalculateHandTypePowerLevel(thisPlayer.m_PlayerHand[0].AttackPower);
            return true;
        }

    }

    /// <summary>
    /// Straight Flush means that all cards' Attack Power are in sequential descending order AND they all have the same suit.
    /// </summary>
    /// <param name="thisPlayer"></param>
    /// <returns>true if both conditions are met; false if not</returns>
    //public bool CheckIfStraightFlush(int attackPowerOf1stCard, string suitOf1stCard, List<CardScriptableObject> thisHand)
    public bool CheckIfStraightFlush(PlayerScriptableObject thisPlayer/*, List<CardScriptableObject> thisHand*/)
    {
        if (CheckIfStraight(thisPlayer) == true && CheckIfFlush(thisPlayer) == true)
        {
            return true;
        }

        return false;
    }

    //public bool CheckIfFourOfAKind(int attackPowerOf1stCard, List<CardScriptableObject> thisHand)
    public bool CheckIfFourOfAKind(PlayerScriptableObject thisPlayer/*, List<CardScriptableObject> sortedHand*/)
    {
        bool four1stNumbers = false;
        bool fourLastNumbers = false;

        four1stNumbers = thisPlayer.m_PlayerHand[0].AttackPower == thisPlayer.m_PlayerHand[1].AttackPower &&
            thisPlayer.m_PlayerHand[1].AttackPower == thisPlayer.m_PlayerHand[2].AttackPower &&
            thisPlayer.m_PlayerHand[2].AttackPower == thisPlayer.m_PlayerHand[3].AttackPower;
        if (four1stNumbers == true)
        {
            thisPlayer.CalculateHandTypePowerLevel(thisPlayer.m_PlayerHand[0].AttackPower);
        }

        fourLastNumbers = thisPlayer.m_PlayerHand[1].AttackPower == thisPlayer.m_PlayerHand[2].AttackPower &&
            thisPlayer.m_PlayerHand[2].AttackPower == thisPlayer.m_PlayerHand[3].AttackPower &&
            thisPlayer.m_PlayerHand[3].AttackPower == thisPlayer.m_PlayerHand[4].AttackPower;
        if (fourLastNumbers == true)
        {
            thisPlayer.CalculateHandTypePowerLevel(thisPlayer.m_PlayerHand[1].AttackPower);
        }

        return four1stNumbers || fourLastNumbers;
    }

    //public bool CheckIfFullHouse(int strongestCardATK, List<CardScriptableObject> sortedHand)
    public bool CheckIfFullHouse(PlayerScriptableObject thisPlayer/*, List<CardScriptableObject> sortedHand*/)
    {
        bool three1st2Last = false;
        bool two1st3Last = false;

        three1st2Last = thisPlayer.m_PlayerHand[0] == thisPlayer.m_PlayerHand[1] &&
            thisPlayer.m_PlayerHand[1] == thisPlayer.m_PlayerHand[2] &&
            thisPlayer.m_PlayerHand[3] == thisPlayer.m_PlayerHand[4];
        if (three1st2Last == true)
        {
            thisPlayer.CalculateHandTypePowerLevel(thisPlayer.m_PlayerHand[0].AttackPower);
        }

        two1st3Last = thisPlayer.m_PlayerHand[0] == thisPlayer.m_PlayerHand[1] &&
            thisPlayer.m_PlayerHand[2] == thisPlayer.m_PlayerHand[3] &&
            thisPlayer.m_PlayerHand[3] == thisPlayer.m_PlayerHand[4];
        if (two1st3Last == true)
        {
            thisPlayer.CalculateHandTypePowerLevel(thisPlayer.m_PlayerHand[2].AttackPower);
        }

        return three1st2Last || two1st3Last;
    }

    //public bool CheckIfThreeOfAKind(int attackPowerOf1stCard, List<CardScriptableObject> thisHand)
    public bool CheckIfThreeOfAKind(PlayerScriptableObject thisPlayer/*, List<CardScriptableObject> thisHand*/)
    {
        bool three1stNumbers = false;
        bool threeMiddleNumbers = false;
        bool threeLastNumbers = false;

        three1stNumbers = thisPlayer.m_PlayerHand[0].AttackPower == thisPlayer.m_PlayerHand[1].AttackPower &&
            thisPlayer.m_PlayerHand[1].AttackPower == thisPlayer.m_PlayerHand[2].AttackPower;
        if (three1stNumbers == true)
        {
            thisPlayer.CalculateHandTypePowerLevel(thisPlayer.m_PlayerHand[0].AttackPower);
        }

        threeMiddleNumbers = thisPlayer.m_PlayerHand[1].AttackPower == thisPlayer.m_PlayerHand[2].AttackPower &&
            thisPlayer.m_PlayerHand[2].AttackPower == thisPlayer.m_PlayerHand[3].AttackPower;
        if (threeMiddleNumbers == true)
        {
            thisPlayer.CalculateHandTypePowerLevel(thisPlayer.m_PlayerHand[1].AttackPower);
        }

        threeLastNumbers = thisPlayer.m_PlayerHand[2].AttackPower == thisPlayer.m_PlayerHand[3].AttackPower &&
            thisPlayer.m_PlayerHand[3].AttackPower == thisPlayer.m_PlayerHand[4].AttackPower;
        if (threeLastNumbers == true)
        {
            thisPlayer.CalculateHandTypePowerLevel(thisPlayer.m_PlayerHand[2].AttackPower);
        }

        return three1stNumbers || threeMiddleNumbers || threeLastNumbers;
    }

    //public bool CheckIf2Pairs(int attackPowerOf1stCard, List<CardScriptableObject> thisHand)
    public bool CheckIf2Pairs(PlayerScriptableObject thisPlayer/*, List<CardScriptableObject> thisHand*/)
    {
        bool twoPairs1st = false;
        bool twoPairsSplit = false;
        bool twoPairsLast = false;

        twoPairs1st = thisPlayer.m_PlayerHand[0].AttackPower == thisPlayer.m_PlayerHand[1].AttackPower &&
            thisPlayer.m_PlayerHand[2].AttackPower == thisPlayer.m_PlayerHand[3].AttackPower;
        if (twoPairs1st == true)
        {
            thisPlayer.CalculateHandTypePowerLevel(thisPlayer.m_PlayerHand[0].AttackPower + thisPlayer.m_PlayerHand[2].AttackPower);
        }

        twoPairsSplit = thisPlayer.m_PlayerHand[0].AttackPower == thisPlayer.m_PlayerHand[1].AttackPower &&
            thisPlayer.m_PlayerHand[3].AttackPower == thisPlayer.m_PlayerHand[4].AttackPower;
        if (twoPairsSplit == true)
        {
            thisPlayer.CalculateHandTypePowerLevel(thisPlayer.m_PlayerHand[0].AttackPower + thisPlayer.m_PlayerHand[3].AttackPower);
        }

        twoPairsLast = thisPlayer.m_PlayerHand[1].AttackPower == thisPlayer.m_PlayerHand[2].AttackPower &&
            thisPlayer.m_PlayerHand[3].AttackPower == thisPlayer.m_PlayerHand[4].AttackPower;
        if (twoPairsLast == true)
        {
            thisPlayer.CalculateHandTypePowerLevel(thisPlayer.m_PlayerHand[1].AttackPower + thisPlayer.m_PlayerHand[3].AttackPower);
        }

        return twoPairs1st || twoPairsSplit || twoPairsLast;
    }

    //public bool CheckIfOnePair(int attackPowerOf1stCard, List<CardScriptableObject> thisHand)
    //public bool CheckIfOnePair(List<CardScriptableObject> thisHand)
    //{
    //    bool onePair1st = false;
    //    bool onePairMiddle1 = false;
    //    bool onePairMiddle2 = false;
    //    bool onePairLast = false;

    //    onePair1st = thisHand[0].AttackPower == thisHand[1].AttackPower;
    //    //if (onePair1st == true)
    //    //{
    //    //    thisHand[0].AttackPower;
    //    //}

    //    onePairMiddle1 = thisHand[1].AttackPower == thisHand[2].AttackPower;

    //    onePairMiddle2 = thisHand[2].AttackPower == thisHand[3].AttackPower;

    //    onePairLast = thisHand[3].AttackPower == thisHand[4].AttackPower;

    //    return onePair1st || onePairMiddle1 || onePairMiddle2 || onePairLast;
    //}
    public bool CheckIfOnePair(PlayerScriptableObject thisPlayer/*, List<CardScriptableObject> sortedHand*/)
    {
        bool onePair1st = false;
        bool onePairMiddle1 = false;
        bool onePairMiddle2 = false;
        bool onePairLast = false;

        //for (int i = 0; i < thisPlayer.m_PlayerHand.Count; i++)
        //{
        //    Debug.Log("thisPlayer.m_PlayerHand[" + i + "].GetCardName = " + thisPlayer.m_PlayerHand[i].GetCardName);
        //}

        onePair1st = thisPlayer.m_PlayerHand[0].AttackPower == thisPlayer.m_PlayerHand[1].AttackPower;
        if (onePair1st == true)
        {
            Debug.Log("one pair 1st!!!!");
            Debug.Log("CardName = " + thisPlayer.m_PlayerHand[0].GetCardName);
            Debug.Log("CardName = " + thisPlayer.m_PlayerHand[1].GetCardName);
            thisPlayer.CalculateHandTypePowerLevel(thisPlayer.m_PlayerHand[0].AttackPower);
        }

        onePairMiddle1 = thisPlayer.m_PlayerHand[1].AttackPower == thisPlayer.m_PlayerHand[2].AttackPower;
        if (onePairMiddle1 == true)
        {
            Debug.Log("CardName = " + thisPlayer.m_PlayerHand[1].GetCardName);
            Debug.Log("CardName = " + thisPlayer.m_PlayerHand[2].GetCardName);
            thisPlayer.CalculateHandTypePowerLevel(thisPlayer.m_PlayerHand[1].AttackPower);
        }

        onePairMiddle2 = thisPlayer.m_PlayerHand[2].AttackPower == thisPlayer.m_PlayerHand[3].AttackPower;
        if (onePairMiddle2 == true)
        {
            Debug.Log("CardName = " + thisPlayer.m_PlayerHand[2].GetCardName);
            Debug.Log("CardName = " + thisPlayer.m_PlayerHand[3].GetCardName);
            thisPlayer.CalculateHandTypePowerLevel(thisPlayer.m_PlayerHand[2].AttackPower);
        }

        onePairLast = thisPlayer.m_PlayerHand[3].AttackPower == thisPlayer.m_PlayerHand[4].AttackPower;
        if (onePairLast == true)
        {
            Debug.Log("CardName = " + thisPlayer.m_PlayerHand[3].GetCardName);
            Debug.Log("CardName = " + thisPlayer.m_PlayerHand[4].GetCardName);
            thisPlayer.CalculateHandTypePowerLevel(thisPlayer.m_PlayerHand[3].AttackPower);
        }

        Debug.Log(thisPlayer.m_PlayerName + " thisPlayer.GetHandTypePowerLevel = " + thisPlayer.GetHandTypePowerLevel);

        return onePair1st || onePairMiddle1 || onePairMiddle2 || onePairLast;
    }
    #endregion

    #endregion
}
