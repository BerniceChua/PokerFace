﻿using System.Collections.Generic;

public interface IPlayerBehaviour
{
    Player m_Player { get; set; }

    void AddCardsToHand(List<CardScriptableObject> allCardsList, List<CardScriptableObject> cardsInHand);
    void AddCardToHand(CardScriptableObject card, List<CardScriptableObject> cardsInHand);
    void ResetPlayerHand(List<CardScriptableObject> cardsInHand);
    void ShowAllCardsInHand();
}