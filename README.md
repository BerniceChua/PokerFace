# PokerFace
![Screenshot Of Lady GaGa's "PokerFace" Music Video](./maxresdefault.jpg)
![Digital Album Cover Of Lady GaGa's "PokerFace"](./PokerFace(DigitalCover).png)

The name is a pun on Lady Gaga's song, "Poker Face".

This version of poker doesn't use the Joker cards.

(The name and the images are just for fun, because of the name "poker". So this is a parody. All copyright belongs to their creators.)

Made with Unity 2019.4.11f1 (Windows)

- itch.io link = <https://bernicechua.itch.io/pokerface>

## How The Code Is Structured:
- Cards: 
  - Each card in a standard deck is already pre-made and represented by a Scriptable Object.  The CardScriptableObject contains the card name (string), card number (int), card suit (enum name converted to a string), and card "Attack Power" (int).  I used "Attack Power", because the card's number is not necessarily how strong it is.  For example, in a standard 52-card deck (no Jokers) Ace cards have the strongest Attack Power, even though their number is only 1.  For the suits, since there are 4 unchanging suits, I turned it into an enum.
- Players / Hands: 
  - Each player has a player name (string), player hand (List<CardScriptableObject>), and hand-type (string) & power-tier (int).  Hand-type is if it's a Flush, Full House, etc. Power-tier is the number associated with the hand-type.  Hand-type-power-level is derived from the Attack Power of which card is dominant.

## Testing Frameworks Used:
- NUnit = (test framework that comes with Unity) -- Test Framework version 1.1.24
- NSubstitute = <https://nsubstitute.github.io/>
  - version 2.0.3 <https://www.nuget.org/packages/NSubstitute/2.0.3>
  - installation instructions here: https://youtu.be/r7VkbV0PRC8?t=1116
- FluentAssertions = <https://github.com/BoundfoxStudios/fluentassertions-unity#unity-package-manager>

## How To Run The Tests:
- `Window` > `General` > `Test Runner`
- In "Test Runner", use the `Edit Mode` tab
- From here, the tests can be run individually or "Run All".
- If one wants to create a new script to test, please make sure to add this in the beginning of the file:
```csharp
using NUnit.Framework;
```
  - if one wants to include NSubstitute and FluentAssertions, please add these as well in the beginning of the file:
```csharp
using NSubstitute;
using FluentAssertions;
```

## Things that I did not make by myself: 
- "Free Playing Cards Pack" by Game Asset Studio <https://assetstore.unity.com/packages/3d/props/tools/free-playing-cards-pack-154780>
- The algorithm used for `Shuffle(List<CardScriptableObject> allListOfCards)` inside `GameLoopManager.cs`' came from this: <https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle>

## Miscellaneous Notes: 
- Right before I left for my trip, I had learned how to use ScriptableObjects from the Unite 2016 talk by Richard Fine; and the Unite 2017 talk by Ryan Hipple
  - https://www.youtube.com/watch?v=6vmRwLYWNRo&t=1626s
  - https://github.com/richard-fine/scriptable-object-demo
  - https://www.youtube.com/watch?v=raQ3iHhE_Kk&t=3s
  - https://github.com/roboryantron/Unite2017
- So I was too enthusiastic about including a new thing that I learned.
- Unfortunately, I realized that I didn't know how to call ScriptableObject for testing.
- Fortunately, I found a nice tutorial video about how to test ScriptableObjects:
  - https://www.youtube.com/watch?app=desktop&v=mg_ZNht0cHE
- When I was going to upload everything, I ran the game because I felt like it.  It was at that point that I noticed a bug that the unit tests didn't catch, because I did not consider that possible scenario.  So for that, I had to create new logic to compensate for it, and new tests to make sure that the logic worked properly.
